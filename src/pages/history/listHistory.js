import React from "react";
import { Grid, Typography, Box } from "@mui/material";
import "assets/styles.css";
import HistoryTable from "components/history/historyTable.js";
function ListHistory() {
  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        gap: "10px",
        margin: "0.9rem",
        justifyContent: "center",
      }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h6">History</Typography>
        </Grid>
        <Grid item xs={12}>
          <HistoryTable />
        </Grid>
      </Grid>
    </Box>
  );
}

export default ListHistory;
