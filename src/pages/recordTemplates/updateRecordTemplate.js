import { Typography, TextField, FormGroup, Box, Grid } from "@mui/material";
import "assets/styles.css";
import React, { useEffect, useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import DraggableFields from "components/field/draggableFields";
import Footer from "components/footer";
import { useRecordTemplateDetails } from "api/recordTemplateApi";
import UpdateRecordTemplateSkeleton from "./skeletons/updateTemplateSkeleton";
import { useUpdateRecordTemplate } from "api/recordTemplateApi";

function UpdateRecordTemplate() {
  const navigate = useNavigate();
  const location = useLocation();

  //Record template fields
  const [recordTemplateTitle, setRecordTemplateTitle] = useState("");
  const [recordTemplateDescription, setRecordTemplateDescription] = useState("");
  const [fields, setFields] = useState([]);

  const { status, data, error, isFetching } = useRecordTemplateDetails(location.state.templateId);
  const { mutate: updateTemplate, isLoading: isUpdating } = useUpdateRecordTemplate();

  useEffect(() => {
    if (data) {
      setRecordTemplateTitle(data?.title);
      setRecordTemplateDescription(data?.description);
      setFields(data?.fields);
    }
  }, [data]);

  const handleOnSubmit = async () => {
    updateTemplate(
      {
        id: location.state.templateId,
        title: recordTemplateTitle,
        description: recordTemplateDescription,
        fields: fields,
      },
      {
        onSuccess: () => {
          navigate("/listRecordTemplate");
        },
      }
    );
  };
  return (
    <>
      {isFetching ? (
        <UpdateRecordTemplateSkeleton />
      ) : (
        <>
          <Box m={4}>
            <Grid>
              <Typography variant="h5">Update Record Template</Typography>
              <FormGroup className="formContainer">
                <Grid>
                  <TextField
                    label="Record template title"
                    variant="standard"
                    type="text"
                    fullWidth
                    className="formField"
                    value={recordTemplateTitle}
                    onChange={(e) => setRecordTemplateTitle(e.target.value)}
                  />
                </Grid>
                <Grid>
                  <TextField
                    label="Record template description"
                    variant="standard"
                    type="text"
                    fullWidth
                    multiline
                    className="formField"
                    value={recordTemplateDescription}
                    onChange={(e) => setRecordTemplateDescription(e.target.value)}
                  />
                </Grid>
              </FormGroup>
              <DraggableFields fields={fields} setFields={setFields} />
            </Grid>
          </Box>
          <Footer action={"Update"} handleOnSubmit={handleOnSubmit} />
        </>
      )}
    </>
  );
}

export default UpdateRecordTemplate;
