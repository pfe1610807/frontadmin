import React, { useState, useEffect } from "react";
import { Grid, Box, Pagination } from "@mui/material";
import { useNavigate } from "react-router-dom";
import RecordTemplateCard from "components/recordTemplateCard";
import Footer from "components/footer";
import UpdateRecordTemplate from "components/recordTemplateCard/updateRecordTemplate";
import RemoveRecordTemplate from "components/recordTemplateCard/removeRecordTemplate";
import PublishRecordTemplate from "components/recordTemplateCard/publishRecordTemplate";
import { useRecordTemplates } from "api/recordTemplateApi";
import RecordTemplateSkeleton from "./skeletons/templateCardSkeleton";
import { useDispatch } from "react-redux";
import { authSliceLogin } from "../../redux/authSlice";
import { toggleDarkMode } from "../../redux/darkModeSlice";
import ErrorContent from "components/actionsData/errorContent";
import NoContent from "components/actionsData/noContent";
import WithPermission from "components/permissions/withPermission";
import PreviewRecordTemplate from "components/recordTemplateCard/previewRecordTemplate";
import { motion } from "framer-motion";

const FooterWithPermission = WithPermission(Footer, [
  "Maintenance_RecordTemplates_Action_Add_View",
  "Maintenance_RecordTemplates_Action_Add_Execute",
]);

function ListRecordTemplate() {
  const navigate = useNavigate();
  const [isPublished, setIsPublished] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [recordTemplates, setRecordTemplates] = useState([]);
  const actionComponents = [
    PreviewRecordTemplate,
    UpdateRecordTemplate,
    RemoveRecordTemplate,
    PublishRecordTemplate,
  ];

  const [page, setPage] = useState(1);
  const itemsPerPage = 10;

  const { data, error, isLoading } = useRecordTemplates();
  const dispatch = useDispatch();
  useEffect(() => {
    const handleMessage = (event) => {
      console.log("Message reçu :", event.data);
      if (event.origin === "http://localhost:3001" && event.data) {
        dispatch(
          authSliceLogin({
            token: event.data.token,
          })
        );
        dispatch(
          toggleDarkMode({
            darkMode: event.data.darkMode.darkMode,
          })
        );
        console.log("data received", event.data);
      }
    };
    window.addEventListener("message", handleMessage);
    return () => {
      window.removeEventListener("message", handleMessage);
    };
  }, []);

  // useEffect(() => {
  //   if (data) {
  //     setRecordTemplates(data);
  //   }
  // }, [data]);
  useEffect(() => {
    if (data) {
      const sortedData = [...data].sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt));
      setRecordTemplates(sortedData);
    }
  }, [data]);

  const handleOnClick = () => {
    navigate("/record-templates/record-template");
  };

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const paginatedRecordTemplates = recordTemplates.slice(startIndex, endIndex);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "space-between",
        padding: "1%",
      }}
    >
      <>
        <Grid container spacing={2}>
          {isLoading ? (
            <>
              {Array.from({ length: itemsPerPage }).map((_, index) => (
                <Grid item xs={12} sm={6} md={4} lg={2.4} key={index}>
                  <RecordTemplateSkeleton actionComponents={actionComponents} />
                </Grid>
              ))}
            </>
          ) : error ? (
            <Grid item xs={12}>
              <ErrorContent />
            </Grid>
          ) : recordTemplates.length === 0 ? (
            <Grid item xs={12}>
              <NoContent
                title="No record templates available"
                body="Currently, there are no record templates available to display."
              />
            </Grid>
          ) : (
            <>
              {paginatedRecordTemplates.map((template) => (
                <Grid item xs={12} sm={6} md={4} lg={2.4} key={template.id}>
                  <motion.div
                    className="box"
                    whileHover={{ scale: 1.05 }}
                    transition={{ type: "spring", stiffness: 200, damping: 10 }}
                  >
                    <RecordTemplateCard
                      template={template}
                      setRecordTemplates={setRecordTemplates}
                      isPublished={isPublished}
                      setIsPublished={setIsPublished}
                      isDeleted={isDeleted}
                      setIsDeleted={setIsDeleted}
                      actionComponents={actionComponents}
                    />
                  </motion.div>
                </Grid>
              ))}
            </>
          )}
          <Grid item>
            {isLoading || error || recordTemplates.length === 0 ? null : (
              <Pagination
                count={Math.ceil(recordTemplates.length / itemsPerPage)}
                page={page}
                onChange={handleChangePage}
                sx={{
                  position: "fixed",
                  bottom: "8%",
                  left: 0,
                  right: 0,
                  display: "flex",
                  justifyContent: "center",
                }}
              />
            )}
          </Grid>
        </Grid>
      </>
      <FooterWithPermission action={"Create Template"} handleOnSubmit={handleOnClick} />
    </Box>
  );
}

export default ListRecordTemplate;
