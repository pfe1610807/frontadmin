import { Typography, TextField, FormGroup, Box, Backdrop, CircularProgress } from "@mui/material";
import React, { useState, useRef, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "assets/styles.css";
import DraggableFields from "components/field/draggableFields";
import Footer from "components/footer";
import html2canvas from "html2canvas";
import { useCreateRecordTemplate } from "api/recordTemplateApi";

function CreateRecordTemplate() {
  const navigate = useNavigate();
  const formRef = useRef(null);

  const [recordTemplateTitle, setRecordTemplateTitle] = useState("");
  const [recordTemplateDescription, setRecordTemplateDescription] = useState("");
  const [fields, setFields] = useState([
    { fieldType: "text", fieldName: "", isMandatory: false, fieldOrder: 0, options: [] },
  ]);
  const { mutate: createTemplate, isLoading: isCreating } = useCreateRecordTemplate();
  const [titleError, setTitleError] = useState("");

  const handleOnSubmit = async () => {
    try {
      if (recordTemplateTitle === "") {
        setTitleError("This field is required !");
        return;
      }
      const canvas2 = await html2canvas(formRef.current, {
        scale: 2,
        mimeType: "image/png",
      });
      const imageData2 = canvas2.toDataURL("image/png");
      createTemplate(
        {
          title: recordTemplateTitle,
          description: recordTemplateDescription,
          fields: fields,
          image: imageData2,
        },
        {
          onSuccess: () => {
            navigate("/listRecordTemplate");
          },
        }
      );
    } catch (error) {
      console.error("Error capturing form as image:", error);
    }
  };
  return (
    <>
      <Box m={4} ref={formRef} sx={{ margin: "20px" }}>
        {isCreating && (
          <Backdrop
            sx={{
              backgroundColor: "rgba(0, 0, 0, 0.15)",
            }}
            open={isCreating}
          >
            <CircularProgress />
          </Backdrop>
        )}
        <Typography variant="h6">Create Record Template</Typography>
        <FormGroup className="formContainer">
          <TextField
            label="Record template title"
            error={titleError}
            helperText={titleError}
            variant="outlined"
            type="text"
            fullWidth
            className="formField"
            onChange={(e) => {
              setTitleError("");
              setRecordTemplateTitle(e.target.value);
            }}
            style={{ marginBottom: "10px" }}
          />
          <TextField
            label="Record template description"
            variant="outlined"
            type="text"
            fullWidth
            multiline
            className="formField"
            onChange={(e) => setRecordTemplateDescription(e.target.value)}
          />
        </FormGroup>
        <DraggableFields fields={fields} setFields={setFields} />
      </Box>
      <Footer action={"Create"} handleOnSubmit={handleOnSubmit} />
    </>
  );
}

export default CreateRecordTemplate;
