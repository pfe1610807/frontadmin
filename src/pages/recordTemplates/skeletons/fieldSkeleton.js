import { FormGroup, Grid, Box, Skeleton } from "@mui/material";
import React from "react";

function FieldSkeleton() {
  return (
    <>
      <FormGroup className="formContainer" style={{ paddingTop: 0, marginBottom: 0 }}>
        <Grid container justifyContent="center" alignItems="flex-end">
          <Skeleton animation="wave" height={15} width="3%" style={{ margin: 10 }} />
        </Grid>
        <Grid container spacing={10} style={{ cursor: "grab" }}>
          <Grid item xs={12} sm={6}>
            <Skeleton animation="wave" height={40} width="80%" />
          </Grid>

          <Grid item xs={12} sm={6}>
            <Skeleton animation="wave" height={40} width="80%" />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Skeleton animation="wave" height={50} width="100%" />
        </Grid>
        <br />
        <Grid className="bottomActions">
          <Skeleton animation="wave" height={30} width="7%" sx={{ marginRight: 1 }} />
          <Skeleton variant="circular" width={30} height={30} sx={{ marginRight: 1 }} />
          <Skeleton variant="circular" width={30} height={30} />
        </Grid>
      </FormGroup>
      <Box display="flex" justifyContent="center">
        <Skeleton variant="circular" width={40} height={40} sx={{ marginTop: 2 }} />
      </Box>
    </>
  );
}

export default FieldSkeleton;
