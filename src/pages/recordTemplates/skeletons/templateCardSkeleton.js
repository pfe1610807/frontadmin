import React from "react";
import { Card, CardContent, CardMedia, CardActions, Divider, Skeleton } from "@mui/material";

function RecordTemplateSkeleton({ actionComponents }) {
  return (
    <Card
      sx={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        marginRight: "3%",
        marginLeft: "3%",
        marginRight: "3%",
        marginLeft: "3%",
      }}
    >
      <CardMedia sx={{ margin: "1%" }}>
        <Skeleton
          sx={{
            width: "100%",
            height: "120px",
            borderRadius: "8px",
          }}
          animation="wave"
          variant="rectangular"
        />
      </CardMedia>
      <Divider sx={{ margin: 0 }} />
      <CardContent sx={{ textAlign: "left", flexGrow: 1 }}>
        <Skeleton animation="wave" height={19} width="60%" style={{ marginBottom: 20 }} />
        <Skeleton animation="wave" height={14} width="80%" style={{ marginBottom: 6 }} />
        <Skeleton animation="wave" height={14} width="90%" style={{ marginBottom: 6 }} />
      </CardContent>
      {actionComponents && actionComponents.length > 0 && (
        <CardActions disableSpacing sx={{ justifyContent: "flex-end", padding: 2 }}>
          {actionComponents.map((_, index) => (
            <Skeleton
              key={index}
              variant="circular"
              width={20}
              height={20}
              sx={{ marginRight: 1 }}
            />
          ))}
        </CardActions>
      )}
    </Card>
  );
}

export default RecordTemplateSkeleton;
