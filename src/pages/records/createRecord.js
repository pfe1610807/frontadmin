import React, { useEffect, useState } from "react";
import { Card, Box, Typography, Grid } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";
import { usecreateAnswer, usecreateRecord } from "api/apiRecord_Answers";
import "assets/styles.css";
import FieldInput from "components/record/fieldInput";
import RecordHeader from "components/record/recordHeader";
import AutoSave from "components/autoSave";
import { fetchFields } from "api/recordTemplateApi";
import Footer from "components/footer";
import { useSelector } from "react-redux";
import JwtParser from "components/auth/JWTParser";
import { useAnswersByRecordId } from "api/apiRecord_Answers";
function CreateRecord() {
  const navigate = useNavigate();

  const location = useLocation();
  const { templateId } = location.state;
  const [recordTitle, setRecordTitle] = useState("");
  const [recordDescription, setRecordDescription] = useState("");

  const [fields, setFields] = useState([]);
  const [answers, setAnswers] = useState({});
  const [fieldResponses, setFieldResponses] = useState({});
  const [record, setRecord] = useState();
  const [data, setData] = useState();

  const { token } = useSelector((state) => state?.auth?.token);
  const decodedToken = JwtParser.parseJwtToken(token);

  const { mutateAsync: createAnswer } = usecreateAnswer();
  const { mutateAsync: createRecord } = usecreateRecord();
  const { data: fetchedAnswers } = useAnswersByRecordId(record?.id);

  const [isSaving, setIsSaving] = useState();

  const handleQuestionAnswer = (fieldId, answerText) => {
    setAnswers((prevAnswers) => ({
      ...prevAnswers,
      [fieldId]: answerText,
    }));
  };
  const handleAnswer = (fieldId, answerText) => {
    setFieldResponses((prevResponses) => ({
      ...prevResponses,
      [fieldId]: answerText,
    }));
  };

  useEffect(() => {
    fetchFields(templateId, setRecordTitle, setRecordDescription, setFields, token, decodedToken);
  }, [templateId]);

  // create the record and the answers
  useEffect(() => {
    const createData = async () => {
      try {
        const userId = decodedToken?.userId;
        const record = await createRecord({ templateId, userId });
        setRecord(record);
        console.log("record : ", record);
        await Promise.all(
          fields.map(async (field) => {
            await createAnswer({
              answerText: answers[field.id],
              questionId: field.id,
              recordId: record.id,
            });
          })
        );
      } catch (error) {
        console.error("Error creating record:", error);
      }
    };
    if (fields.length > 0) createData();
  }, [fields]);

  // Get the answer's by Record
  useEffect(() => {
    if (fetchedAnswers) {
      setData(fetchedAnswers);
      setFieldResponses(
        fetchedAnswers.reduce((acc, answer) => {
          if (answer.field.fieldType) {
            acc[answer.field.id] = answer.answerText;
          }
          return acc;
        }, {})
      );
    }
  }, [record, fetchedAnswers]);
  const handleSubmit = () => {
    navigate("/records");
  };
  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        gap: "10px",
        margin: "2%",
        justifyContent: "center",
      }}
    >
      <Grid container spacing={2}>
        <Grid item>
          <Typography variant="h5">Create Record</Typography>
        </Grid>
        <Grid item>
          <AutoSave
            fieldResponses={fieldResponses}
            isSaving={isSaving}
            setIsSaving={setIsSaving}
            data={data}
          />
        </Grid>
        <Grid item xs={12}>
          <RecordHeader recordTitle={recordTitle} recordDescription={recordDescription} />
        </Grid>
        <Grid item xs={12}>
          {fields.length > 0 &&
            fields.map((field) => (
              <Card key={field.id} sx={{ marginBottom: 2, padding: 2, fontSize: "1.2rem" }}>
                <FieldInput
                  field={field}
                  handleAnswerChange={(fieldId, answerText) => {
                    handleQuestionAnswer(fieldId, answerText);
                    handleAnswer(fieldId, answerText);
                  }}
                  setIsSaving={setIsSaving}
                  response={fieldResponses[field.id]}
                />
              </Card>
            ))}
        </Grid>
      </Grid>
      <Footer action={"Finish"} handleOnSubmit={handleSubmit} />
    </Box>
  );
}
export default CreateRecord;
