import React, { useState, useEffect } from "react";
import { Grid, Box, Pagination } from "@mui/material";
import { usePublishedRecordTemplates } from "api/recordTemplateApi";
import SelectTemplate from "components/record/actions/selectTemplate";
import RecordTemplateCard from "components/recordTemplateCard/index";
import RecordTemplateSkeleton from "pages/recordTemplates/skeletons/templateCardSkeleton";
import ErrorContent from "components/actionsData/errorContent";
import NoContent from "components/actionsData/noContent";
import { useSelector } from "react-redux";

function ChooseRecordTemplate() {
  const [recordTemplates, setRecordTemplates] = useState([]);
  const [page, setPage] = useState(1);
  const itemsPerPage = 10;
  const actionComponents = [SelectTemplate];
  const { status, data, error, isFetching, refetch } = usePublishedRecordTemplates();
  const { socket } = useSelector((state) => state.socket);
  useEffect(() => {
    if (data) {
      const sortedData = [...data].sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt));
      setRecordTemplates(sortedData);
    }
  }, [data]);

  useEffect(() => {
    if (socket) {
      socket.on("notification", (message) => {
        refetch();
      });
    }
  }, [socket]);

  const handleChangePage = (event, value) => {
    setPage(value);
  };

  const startIndex = (page - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const paginatedRecordTemplates = recordTemplates.slice(startIndex, endIndex);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "space-between",
        padding: "1%",
      }}
    >
      <Grid container spacing={2}>
        {isFetching ? (
          <>
            {Array.from({ length: itemsPerPage }).map((_, index) => (
              <Grid item xs={12} sm={6} md={4} lg={2.4} key={index}>
                <RecordTemplateSkeleton
                  actionComponents={[SelectTemplate]}
                  isFetching={isFetching}
                />
              </Grid>
            ))}
          </>
        ) : error ? (
          <ErrorContent />
        ) : recordTemplates.length === 0 ? (
          <NoContent
            title="No Record Template available"
            body="Currently, there are no Record Template available to display."
          />
        ) : (
          <>
            {paginatedRecordTemplates.map((template) => (
              <Grid item xs={12} sm={6} md={4} lg={2.4} key={template.id}>
                <RecordTemplateCard
                  template={template}
                  key={template.id}
                  actionComponents={actionComponents}
                />
              </Grid>
            ))}
          </>
        )}

        <Grid item>
          {isFetching ? null : (
            <Pagination
              count={Math.ceil(recordTemplates.length / itemsPerPage)}
              page={page}
              onChange={handleChangePage}
              sx={{
                position: "fixed",
                bottom: "2%",
                left: "50%",
                transform: "translateX(-50%)", // Adjusts for centering
                zIndex: 999, // Ensures it appears above other content
              }}
            />
          )}
        </Grid>
      </Grid>
    </Box>
  );
}

export default ChooseRecordTemplate;
