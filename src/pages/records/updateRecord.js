import React, { useEffect, useState } from "react";
import { Container, Divider, Card, CardContent, Typography, Grid, Box } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";
import { useAnswer } from "api/apiRecord_Answers";
import FieldInput from "components/record/fieldInput";
import RecordHeader from "components/record/recordHeader";
import AutoSave from "components/autoSave";
import { fetchFields } from "api/recordTemplateApi";
import Footer from "components/footer";
import UpdateRecordSkeleton from "./skeletonrecord/updateRecordSkeleton";
import { useUpdateAnswer } from "api/apiRecord_Answers";
import { useSelector } from "react-redux";
import JwtParser from "components/auth/JWTParser";

function UpdateRecord() {
  const navigate = useNavigate();
  const [recordTitle, setRecordTitle] = useState("");
  const [fields, setFields] = useState([]);
  const [recordDescription, setRecordDescription] = useState("");
  const location = useLocation();
  const [fieldResponses, setFieldResponses] = useState({});
  const [data, setData] = useState([]);
  const [isSaving, setIsSaving] = useState();
  const { status, data: fetchedData, error, isFetching } = useAnswer(location.state.recordId);
  const updateAnswerMutation = useUpdateAnswer();

  const recordTemplateId = location.state.recordTemplateId;
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  useEffect(() => {
    const fetchData = async () => {
      await fetchFields(
        recordTemplateId,
        setRecordTitle,
        setRecordDescription,
        setFields,
        token.token,
        decodedToken
      );
      if (fetchedData) {
        setData(fetchedData);
        setFieldResponses(
          fetchedData.reduce((acc, answer) => {
            if (answer.field.fieldType) {
              acc[answer.field.id] = answer.answerText;
            }
            return acc;
          }, {})
        );
      }
    };

    fetchData();
  }, [recordTemplateId, fetchedData]);

  const handleQuestionAnswer = (answerId, answerText) => {
    setFieldResponses((prevResponses) => ({
      ...prevResponses,
      [answerId]: answerText,
    }));
  };

  const handleSubmit = async () => {
    try {
      await Promise.all(
        data.map(async (answer) => {
          const answerId = answer.id;
          const answerText = fieldResponses[answer.field.id];
          await updateAnswerMutation.mutateAsync({ answerId, answerText });
        })
      );
      navigate("/records");
    } catch (error) {
      console.error("Error updating answers:", error);
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        gap: "10px",
        margin: "2%",
        justifyContent: "center",
      }}
    >
      <Grid container spacing={2}>
        <Grid item>
          <Typography variant="h5">Update Record</Typography>
        </Grid>
        <Grid item>
          <AutoSave
            fieldResponses={fieldResponses}
            isSaving={isSaving}
            setIsSaving={setIsSaving}
            data={data}
          />
        </Grid>
        {isFetching ? (
          <Grid item xs={12}>
            <UpdateRecordSkeleton />
          </Grid>
        ) : (
          <>
            {error && (
              <Grid item xs={12}>
                <Typography variant="body2" color="error">
                  {error.message}
                </Typography>
              </Grid>
            )}
            {!isFetching && !error && (
              <>
                <Grid item xs={12}>
                  <RecordHeader recordTitle={recordTitle} recordDescription={recordDescription} />
                </Grid>
                <Grid item xs={12}>
                  {fields.map((field) => (
                    <Card key={field.id} sx={{ marginBottom: 2, padding: 2, fontSize: "1.2rem" }}>
                      <FieldInput
                        field={field}
                        handleAnswerChange={(answerId, answerText) => {
                          handleQuestionAnswer(answerId, answerText);
                        }}
                        setIsSaving={setIsSaving}
                        response={fieldResponses[field.id] || ""}
                      />
                    </Card>
                  ))}
                </Grid>
              </>
            )}
          </>
        )}
      </Grid>
      <Footer action={"Finish"} handleOnSubmit={handleSubmit} />
    </Box>
  );
}

export default UpdateRecord;
