import { FormGroup, Grid, Box, Skeleton } from "@mui/material";
import React from "react";

function FieldImputSkeleton() {
  return (
    <>
      <FormGroup className="formContainer" style={{ paddingTop: 0, marginBottom: 0 }}>
        <Grid container spacing={10} style={{ cursor: "grab" }}>
          <Grid item xs={12} sm={6}>
            <Skeleton animation="wave" height={40} width="80%" />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Skeleton animation="wave" height={50} width="100%" />
        </Grid>
        <Grid item xs={12} sm={6}>
          <Skeleton animation="wave" height={50} width="100%" />
        </Grid>
        <br />
      </FormGroup>
    </>
  );
}

export default FieldImputSkeleton;
