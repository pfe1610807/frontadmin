import React from "react";
import { Skeleton } from "@mui/material";

const TableSkeleton = () => {
  const columns = [
    {
      field: "column1",
      flex: 1,
      headerName: <Skeleton animation="wave" height={30} width={150} />,
      renderCell: () => (
        <Skeleton animation="wave" height={20} style={{ marginBottom: 8, width: "60%" }} />
      ),
    },
    {
      field: "column2",
      flex: 1,
      headerName: <Skeleton animation="wave" height={30} width={150} />,
      renderCell: () => (
        <Skeleton animation="wave" height={20} style={{ marginBottom: 8, width: "60%" }} />
      ),
    },
    {
      field: "column3",
      flex: 1,
      headerName: <Skeleton animation="wave" height={30} width={150} />,
      renderCell: () => (
        <Skeleton animation="wave" height={20} style={{ marginBottom: 8, width: "60%" }} />
      ),
    },
    {
      field: "column4",
      flex: 1,
      headerName: <Skeleton animation="wave" height={30} width={150} />,
      renderCell: () => (
        <Skeleton animation="wave" height={20} style={{ marginBottom: 8, width: "60%" }} />
      ),
    },
    {
      field: "column5",
      flex: 1,
      headerName: <Skeleton animation="wave" height={30} width={150} />,
      renderCell: () => (
        <Skeleton animation="wave" height={20} style={{ marginBottom: 8, width: "60%" }} />
      ),
    },
    {
      field: "column6",
      flex: 1,
      headerName: <Skeleton animation="wave" height={30} width={150} />,
      renderCell: () => (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            width: "60%",
          }}
        >
          <Skeleton variant="circular" width={30} height={30} />
          <Skeleton variant="circular" width={30} height={30} />
          <Skeleton variant="circular" width={30} height={30} />
        </div>
      ),
    },
  ];

  return (
    <table style={{ width: "100%", marginTop: "50px" }}>
      <thead>
        <tr>
          {columns.map((column) => (
            <th key={column.field}>{column.headerName}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {Array(6)
          .fill(0)
          .map((_, index) => (
            <tr key={index}>
              {columns.map((column) => (
                <td key={`${column.field}-${index}`}>{column.renderCell()}</td>
              ))}
            </tr>
          ))}
      </tbody>
    </table>
  );
};

export default TableSkeleton;

// import React from "react";
// import { Box, Grid, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material";
// import { Skeleton } from "@mui/material";

// const TableSkeleton = () => {
//   return (
//     <Table>
//       <TableHead>
//         <TableRow>
//           <TableCell>
//             <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//           </TableCell>
//           <TableCell>
//             <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//           </TableCell>
//           <TableCell>
//             <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//           </TableCell>
//           <TableCell>
//             <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//           </TableCell>

//           <TableCell>
//             <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//           </TableCell>

//           <TableCell>
//             <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//           </TableCell>
//         </TableRow>
//       </TableHead>
//       <TableBody>
//         {Array(6)
//           .fill(0)
//           .map((_, index) => (
//             <Grid key={index}>
//               <TableRow key={index}>
//                 <TableCell>
//                   <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//                 </TableCell>
//                 <TableCell>
//                   <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//                 </TableCell>
//                 <TableCell>
//                   <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//                 </TableCell>
//                 <TableCell>
//                   <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//                 </TableCell>
//                 <TableCell>
//                   <Skeleton animation="wave" variant="rectangular" height={30} width={150} />
//                 </TableCell>
//                 <TableCell>
//                   <Box display="flex" justifyContent="space-between" spacing={10}>
//                     <Skeleton variant="circular" width={30} height={30} />
//                     <Skeleton variant="circular" width={30} height={30} />
//                     <Skeleton variant="circular" width={30} height={30} />
//                   </Box>
//                 </TableCell>
//               </TableRow>
//             </Grid>
//           ))}
//       </TableBody>
//     </Table>
//   );
// };
// export default TableSkeleton;
