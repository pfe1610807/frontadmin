import { FormGroup, Box, Grid, Skeleton } from "@mui/material";
import "assets/styles.css";
import React from "react";
import FieldImputSkeleton from "./fieldImputSkeleton";

function UpdateRecordSkeleton() {
  return (
    <>
      <Box m={4}>
        <Grid>
          <Skeleton animation="wave" height={25} width="20%" style={{ marginBottom: 10 }} />
          <FormGroup className="formContainer">
            <Grid>
              <Skeleton animation="wave" height={10} width="30%" />
            </Grid>
            <Grid>
              <Skeleton animation="wave" height={30} width="100%" style={{ marginBottom: 5 }} />
            </Grid>
          </FormGroup>
          <FieldImputSkeleton />
        </Grid>
      </Box>
    </>
  );
}

export default UpdateRecordSkeleton;
