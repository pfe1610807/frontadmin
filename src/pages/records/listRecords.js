import React from "react";
import { Grid, Typography, Box } from "@mui/material";
import "assets/styles.css";
import { useNavigate } from "react-router-dom";
import Footer from "components/footer";
import RecordTable from "components/record/recordTable";
function ListRecords() {
  const navigate = useNavigate();

  const handleOnSubmit = () => {
    navigate("/available-record-templates");
  };
  return (
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        gap: "10px",
        margin: "0.9rem",
        justifyContent: "center",
      }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h6">Records</Typography>
        </Grid>
        <Grid item xs={12}>
          <RecordTable />
        </Grid>
      </Grid>

      <Footer action={"View Templates"} handleOnSubmit={handleOnSubmit} disabled={false} />
    </Box>
  );
}

export default ListRecords;
