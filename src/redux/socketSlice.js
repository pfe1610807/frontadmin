import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isInitialized: false,
  isConnected: null,
  isReconnecting: 0,
  socket: null,
};

const socketSlice = createSlice({
  name: "socket",
  initialState,
  reducers: {
    initializeSocket(state) {
      state.isInitialized = true;
    },
    setConnectedStatus(state, action) {
      state.isConnected = action.payload.isConnected;
      console.log("state.isConnected", action.payload.isConnected);
    },
    setIsReconnectingStatus(state, action) {
      state.isReconnecting = state.isReconnecting + 1;
      console.log("state.isReconnecting", state.isReconnecting + 1);
    },
    saveSocket(state, action) {
      state.socket = action.payload.socket;
    },
  },
});

export const { initializeSocket, setConnectedStatus, setIsReconnectingStatus, saveSocket } =
  socketSlice.actions;

export default socketSlice.reducer;
