import {
  initializeSocket,
  setConnectedStatus,
  saveSocket,
  setIsReconnectingStatus,
} from "../socketSlice";
import io from "socket.io-client";

let socketInstance = null;

const socketMiddleware = (store) => (next) => (action) => {
  if (action.type === "socket/initializeSocket" && !socketInstance) {
    socketInstance = io(action.payload.url, {
      reconnectionDelay: 5000,
      reconnectionDelayMax: 5000,
    });
    store.dispatch(saveSocket({ socket: socketInstance }));

    socketInstance.on("connect", () => {
      console.log("Socket connected:", socketInstance.id);
      store.dispatch(setConnectedStatus({ isConnected: true }));
    });

    socketInstance.on("reconnect_attempt", () => {
      console.log("Attempting to reconnect...");
      store.dispatch(setIsReconnectingStatus());
    });

    socketInstance.on("reconnect", () => {
      console.log("Reconnected successfully on attempt");
      store.dispatch(setIsReconnectingStatus({ isReconnecting: false }));
      store.dispatch(setConnectedStatus({ isConnected: true }));
    });

    socketInstance.on("disconnect", () => {
      console.log("Disconnected from server");
      store.dispatch(setConnectedStatus({ isConnected: false }));
    });

    store.dispatch(initializeSocket());
  }

  return next(action);
};

export default socketMiddleware;
