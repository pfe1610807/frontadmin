import { createSlice } from "@reduxjs/toolkit";
export const authSlice = createSlice({
  name: "auth",
  initialState: {
    token: null,
  },
  reducers: {
    authSliceLogin: (state, action) => {
      state.token = action.payload;
    },
    signOut: (state) => {
      state.token = null;
    },
  },
});
export const { authSliceLogin, signOut } = authSlice.actions;
export default authSlice.reducer;
