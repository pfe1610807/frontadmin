import { combineReducers, configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";
import { persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from "redux-persist";
import darkMode from "./darkModeSlice";
import authSlice from "./authSlice";
import socket from "./socketSlice";
import socketMiddleware from "./middleware/socketMiddleware";
// qui permet de combiner plusieurs reducers
const reducers = combineReducers({
  darkMode,
  auth: authSlice,
  socket,
});
//Configuration de redux-persist
const persistConfig = {
  key: "root", // Clé de stockage dans le localStorage
  storage, // Configuration du stockage
  blacklist: ["socket"],
};

const persistedReducer = persistReducer(persistConfig, reducers);
const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== "production",
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(socketMiddleware),
});

export default store;
