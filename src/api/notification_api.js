import { useMutation, useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useSelector } from "react-redux";
import useConfig from "hooks/useConfig";
const API_BASE_URL = "http://localhost:8081";

// GET ONE
export const useNotificationsByUser = (id) => {
  // const config = useConfig();
  const { token } = useSelector((state) => state.auth);
  return useQuery({
    queryKey: ["notifications", id],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/notifications/${id}`, {
        headers: {
          Authorization: token.token,
        },
      });
      return data;
    },
    enabled: !!id,
  });
};

//Read one notification
export const markNotificationAsRead = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state.auth);
  return useMutation({
    mutationKey: ["update-notification-is-read"],
    mutationFn: async (notificationId) => {
      const response = await axios.put(`${API_BASE_URL}/notifications/${notificationId}`, null, {
        headers: {
          Authorization: token.token,
        },
      });
      return response.data;
    },
  });
};

// Mark all notifications as read
export const markAllNotificationsAsRead = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state.auth);
  return useMutation({
    mutationKey: ["mark-all-notifications-as-read"],
    mutationFn: async (userId) => {
      const response = await axios.patch(`${API_BASE_URL}/notifications/user/${userId}`, null, {
        headers: {
          Authorization: token.token,
        },
      });
      return response.data;
    },
  });
};
