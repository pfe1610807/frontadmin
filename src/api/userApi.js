import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { useSelector } from "react-redux";
import useConfig from "hooks/useConfig";
const API_BASE_URL = "http://localhost:8081";

//Get By Id
export const useUser = (id) => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  return useQuery({
    queryKey: ["user"],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/users/${id}`, {
        headers: {
          Authorization: token.token,
        },
      });
      return data;
    },
    enabled: !!id,
  });
};
// GET ALL
export const useUsers = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  return useQuery({
    queryKey: ["Users"],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/users`, {
        headers: {
          Authorization: token.token,
        },
      });
      return data;
    },
  });
};
