import { useMutation, useQuery } from "@tanstack/react-query";
import axios from "axios";
import JwtParser from "components/auth/JWTParser";
import { useSelector } from "react-redux";
// import useConfig from "hooks/useConfig";
const API_BASE_URL = "http://localhost:8080";

//createRecord
export const usecreateRecord = () => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  // const config = useConfig();
  return useMutation({
    mutationFn: async (recordTemplateData) => {
      const response = await axios.post(
        `${API_BASE_URL}/records/${recordTemplateData.templateId}`,
        { userId: recordTemplateData.userId },
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
    mutationKey: ({ recordTemplateId }) => ["createRecordTemplate", recordTemplateId],
  });
};

// Get Record isOpen (EDIT)
export const useRecord = () => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  // const config = useConfig();
  return useQuery({
    queryKey: ["listrecords"],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/records`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
  });
};

// GET RECORS BY USER
export const useRecordByUserId = (id) => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useQuery({
    queryKey: ["list-records-by-user-id"],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/records/user/${id}`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
  });
};
//Update Answer
export const useUpdateAnswer = () => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  // const config = useConfig();
  return useMutation({
    mutationFn: async ({ answerId, answerText }) => {
      const response = await axios.put(
        `${API_BASE_URL}/records/answers/${answerId}`,
        {
          answerText,
        },
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
    mutationKey: ({ answerId }) => ["Updateanswers", answerId],
  });
};
// Get Answer By Id
export const useAnswer = (id) => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  // const config = useConfig();
  return useQuery({
    queryKey: ["answers"],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/records/answers/${id}`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
  });
};
//Signed Record
export const useSigneRecord = () => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useMutation({
    mutationKey: ["signed-record"],
    mutationFn: async (id) => {
      const response = await axios.patch(
        `${API_BASE_URL}/records/${id}`,
        {},
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
  });
};

//Export Pdf
export const useExportRecordTemplatePdf = () => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useMutation({
    mutationKey: ["export-record"],
    mutationFn: async (record) => {
      console.log("record test", record);
      let response;
      try {
        response = await axios.get(`${API_BASE_URL}/records/download-pdf/${record.id}`, {
          responseType: "blob",
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        });

        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        const now = new Date();
        const dateExport = now.toISOString().split("T")[0];
        const hours = String(now.getHours()).padStart(2, `"0"`);
        const minutes = String(now.getMinutes()).padStart(2, `"0"`);
        const timeExport = `${hours}:${minutes}`;
        link.setAttribute(
          "download",
          `${record.recordTemplate.title}_${dateExport}_${timeExport}.pdf`
        );
        document.body.appendChild(link);
        link.click();
        link.parentNode.removeChild(link);
      } catch (error) {
        console.error("Error exporting PDF:", error);
        throw error;
      }
      return response.data;
    },
  });
};
//Answer
//createAnswer

export const usecreateAnswer = () => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  // const config = useConfig();
  return useMutation({
    mutationFn: async ({ answerText, questionId, recordId }) => {
      const response = await axios.post(
        `${API_BASE_URL}/records/answers`,
        {
          answerText: answerText,
          field: { id: questionId },
          record: { id: recordId },
        },
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
    mutationKey: ({ recordId }) => ["createAnswer", recordId],
  });
};

export const useAnswersByRecordId = (recordId) => {
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  // const config = useConfig();
  return useQuery({
    queryKey: ["Answers-ByRecordId"],
    queryFn: async () => {
      const { data } = await axios.get(`${API_BASE_URL}/records/answers/${recordId}`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
    enabled: !!recordId,
  });
};
