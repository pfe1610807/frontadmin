import { useMutation, useQuery } from "@tanstack/react-query";
import axios from "axios";
import JwtParser from "components/auth/JWTParser";
import useConfig from "hooks/useConfig";
import { useSelector } from "react-redux";
const API_BASE_URL = "http://localhost:8080";

// GET ALL
export const useRecordTemplates = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useQuery({
    queryKey: ["record-templates"],
    queryFn: async () => {
      const { data } = await axios.get(`${config.RECORD_SERVICE_URL}/record-templates`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
  });
};

// GET ONE
export const useRecordTemplateDetails = (id) => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useQuery({
    queryKey: ["record-template", id],
    queryFn: async () => {
      const { data } = await axios.get(`${config.RECORD_SERVICE_URL}/record-templates/${id}`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
    enabled: !!id,
  });
};

// GET PUBLISHED
export const usePublishedRecordTemplates = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useQuery({
    queryKey: ["published-record-templates"],
    queryFn: async () => {
      const { data } = await axios.get(`${config.RECORD_SERVICE_URL}/record-templates/published`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return data;
    },
  });
};

// CREATE
export const useCreateRecordTemplate = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useMutation({
    mutationKey: ["create-record-template"],
    mutationFn: async (recordTemplateData) => {
      const response = await axios.post(
        `${config.RECORD_SERVICE_URL}/record-templates`,
        recordTemplateData,
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
  });
};

// UPDATE
export const useUpdateRecordTemplate = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useMutation({
    mutationKey: ["update-record-template"],
    mutationFn: async (recordTemplateData) => {
      const response = await axios.put(
        `${config.RECORD_SERVICE_URL}/record-templates/${recordTemplateData.id}`,
        recordTemplateData,
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
  });
};

// PUBLISH
export const usePublishRecordTemplate = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useMutation({
    mutationKey: ["publish-record-template"],
    mutationFn: async (recordTemplateId) => {
      const response = await axios.patch(
        `${config.RECORD_SERVICE_URL}/record-templates/${recordTemplateId}`,
        {},
        {
          headers: {
            Authorization: token.token,
            Username: decodedToken.sub,
          },
        }
      );
      return response.data;
    },
  });
};

// DELETE
export const useDeleteRecordTemplate = () => {
  const config = useConfig();
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  return useMutation({
    mutationKey: ["delete-record-template"],
    mutationFn: async (id) => {
      const response = await axios.delete(`${config.RECORD_SERVICE_URL}/record-templates/${id}`, {
        headers: {
          Authorization: token.token,
          Username: decodedToken.sub,
        },
      });
      return response.data;
    },
  });
};

// Fields
// GET ALL
export const fetchFields = async (
  // templateId,
  // setRecordTitle,
  // setRecordDescription,
  // setFields,
  // token,
  // decodedToken
) => {
  // return axios
  //   .get(`${API_BASE_URL}/record-templates/${templateId}`, {
  //     headers: {
  //       Authorization: token,
  //       Username: decodedToken.sub,
  //     },
  //   })
  //   .then((res) => {
  //     const data = res.data;
  //     setRecordTitle(data.title);
  //     setRecordDescription(data.description);

  //     const questions = data.fields.map((field) => {
  //       for (let fieldType of Object.values(field.fieldType)) {
  //         if (field.fieldType === fieldType) {
  //           return axios
  //             .get(`${API_BASE_URL}/record-templates/fields/${field.id}/options`, {
  //               headers: {
  //                 Authorization: token,
  //                 Username: decodedToken.sub,
  //               },
  //             })
  //             .then((res) => {
  //               const optionsData = res.data;
  //               return {
  //                 ...field,
  //                 options: optionsData.map((option) => ({
  //                   id: option.id,
  //                   optionName: option.optionName,
  //                 })),
  //               };
  //             })
  //             .catch((err) => {
  //               console.log(err);
  //             });
  //         }
  //       }
  //       return field;
  //     });
  //     Promise.all(questions).then((fields) => {
  //       setFields(fields);
  //     });
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //   });
};
