import { useState, useEffect } from "react";
import { getData } from "../utils/getData";
export default function useConfig() {
  const [data, setData] = useState();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await getData("/app_urls.json", "json");
      setData(response);
    } catch (error) {
      console.error(error);
    }
  };
  return data;
}
