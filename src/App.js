import React, { useEffect, useRef } from "react";
import { Routes, Route } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import theme from "assets/theme";
import themeDark from "assets/theme-dark";
import routes from "routes";
import "assets/styles.css";
import Navbar from "components/Navbar";
import { useDispatch, useSelector } from "react-redux";
import { toggleDarkMode } from "./redux/darkModeSlice";
import { initializeSocket } from "./redux/socketSlice";
import useToastNotification from "components/toast";
import useConfig from "hooks/useConfig";
import { authSliceLogin } from "./redux/authSlice";

export default function App() {
  const isTrue = true;

  // Routes
  const getRoutes = (allRoutes) =>
    allRoutes.flatMap((route, index) => {
      if (route.collapse) {
        return getRoutes(route.collapse);
      }
      if (route.route) {
        return <Route exact path={route.route} element={route.component} key={index} />;
      }
      return null;
    });

  const dispatch = useDispatch();

  // Redux states
  const { darkMode } = useSelector((state) => state.darkMode);
  const { token } = useSelector((state) => state?.auth);
  const { isConnected } = useSelector((state) => state.socket);
  const { isReconnecting } = useSelector((state) => state.socket);

  // Initializing socket connection
  const config = useConfig();
  useEffect(() => {
    if (config !== undefined) dispatch(initializeSocket({ url: config.SOCKET_URL }));
  }, [config]);

  useToastNotification("Connected to server!", "success", isConnected === true);
  useToastNotification("Disconnected from server!", "error", isConnected === false);

  const previousReconnectCount = useRef(isReconnecting);

  useToastNotification(
    "Attempting to reconnect to server!",
    "warning",
    isReconnecting >= previousReconnectCount.current && isReconnecting !== 0
  );

  // Handle DarkMode
  const handleDarkMode = () => dispatch(toggleDarkMode({ darkMode: !darkMode }));
  useEffect(() => {
    if (darkMode) {
      document.body.classList.add("dark-mode");
      document.body.classList.remove("light-mode");
    } else {
      document.body.classList.add("light-mode");
      document.body.classList.remove("dark-mode");
    }
  }, [darkMode]);

  // Logout if token is null
  useEffect(() => {
    const timeout = setTimeout(() => {
      if (token === null || token === "") {
        window.location.href = "http://localhost:3001";
      }
    }, 800);
    return () => clearTimeout(timeout);
  }, [token]);

  // Event listener, for testing
  window.addEventListener("setToken", (event) => {
    console.log("token : ", event?.detail);
    dispatch(
      authSliceLogin({
        token: event?.detail,
      })
    );
  });
  return (
    <ThemeProvider theme={darkMode ? themeDark : theme}>
      <CssBaseline />
      <Navbar
        brandName="Admin"
        routes={routes}
        darkMode={darkMode}
        toggleDarkMode={handleDarkMode}
        isTrue={isTrue}
      />

      <Routes>{getRoutes(routes)}</Routes>
    </ThemeProvider>
  );
}
