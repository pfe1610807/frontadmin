const DataGrid = {
  styleOverrides: {
    root: {
      "& .MuiDataGrid-cellContent": {
        color: "white!important",
      },

      "& .MuiDataGrid-columnHeaderTitle": {
        color: "white!important",
      },
    },
  },
};

export default DataGrid;
