const appBar = {
  defaultProps: {
    color: "transparent",
  },

  styleOverrides: ({ darkMode }) => ({
    root: {
      boxShadow: "none",
      position: "relative",
      "&::before": {
        content: '""',
        position: "absolute",
        top: 0,
        left: 0,
        width: "50%",
        height: "100%",
        background: darkMode ? "#747474" : "#f5f5f5",
      },
      "&::after": {
        content: '""',
        position: "absolute",
        top: 0,
        right: 0,
        width: "50%",
        height: "100%",
        background: darkMode ? "#747474" : "#f5f5f5",
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        paddingRight: "10px",
      },
    },
  }),
};

export default appBar;
