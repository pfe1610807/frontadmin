import colors from "assets/theme-dark/base/colors";

const { transparent } = colors;

const iconButton = {
  styleOverrides: {
    root: {
      backgroundColor: transparent.main,
    },
  },
};

export default iconButton;
