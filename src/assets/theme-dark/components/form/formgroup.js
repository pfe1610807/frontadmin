const fromgroup = {
  styleOverrides: {
    root: {
      backgroundColor: "#333333",
      boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
      border: "none",
    },
  },
};

export default fromgroup;
