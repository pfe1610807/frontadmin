import colors from "assets/theme-dark/base/colors";

const { transparent, info } = colors;

const textField = {
  styleOverrides: {
    root: {
      backgroundColor: transparent.main,
      border: "none",
      color: info.main,
    },
  },
};

export default textField;
