import colors from "assets/theme-dark/base/colors";
const { background } = colors;
const paper = {
  styleOverrides: {
    root: {
      // backgroundColor: background.card,
      backgroundColor: "#292929!important",
      boxShadow: "0px -4px 4px rgba(0, 0, 0, 0.2)",
    },
  },
};

export default paper;
