import colors from "assets/theme-dark/base/colors";
import borders from "assets/theme-dark/base/borders";
import boxShadows from "assets/theme-dark/base/boxShadows";

// Material Dashboard 2 React Helper Function
import rgba from "assets/theme-dark/functions/rgba";

const { black, background, text } = colors;
const { borderWidth, borderRadius } = borders;
const { md } = boxShadows;

const card = {
  styleOverrides: {
    root: {
      display: "flex",
      flexDirection: "column",
      position: "relative",
      minWidth: 0,
      wordWrap: "break-word",
      backgroundImage: "none",
      // backgroundColor: background.card,
      backgroundColor: "#333333!important",
      backgroundClip: "border-box",
      border: `${borderWidth[0]} solid ${rgba(black.main, 0.125)}`,
      borderRadius: borderRadius.xl,
      // boxShadow: md,
      overflow: "visible",
      color: "#ffffff",
      boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
    },
  },
};

export default card;
