import React from "react";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const CustomDataGrid = {
  styleOverrides: {
    root: {
      "& .MuiDataGrid-cellContent": {
        color: "black!important",
      },

      "& .MuiDataGrid-columnHeader--sortable": {
        color: "black!important",
      },
    },
  },
};

export default CustomDataGrid;
