const link = {
  defaultProps: {
    underline: "none",
    color: "#42647b!important",
  },
};

export default link;
