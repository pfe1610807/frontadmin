import colors from "assets/theme/base/colors";

const { white } = colors;

const formGroup = {
  styleOverrides: {
    root: {
      backgroundColor: white.main,
      boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
      border: "none",
    },
  },
};

export default formGroup;
