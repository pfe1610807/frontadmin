import colors from "assets/theme/base/colors";

const { white } = colors;
const bottomNavigation = {
  styleOverrides: {
    root: {
      backgroundColor: white.main,
    },
  },
};

export default bottomNavigation;
