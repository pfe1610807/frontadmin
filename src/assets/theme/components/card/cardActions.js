const cardActions = {
  styleOverrides: {
    root: {
      backgroundColor: "#F7F7F7!important",
      boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
      borderRadius: "8px",
    },
  },
};

export default cardActions;
