import ListRecordTemplate from "pages/recordTemplates/listRecordTemplate";
import CreateRecord from "pages/records/createRecord";
import ListRecords from "pages/records/listRecords";
import ChooseRecordTemplate from "pages/records/chooseRecordTemplate";
import UpdateRecordTemplate from "pages/recordTemplates/updateRecordTemplate";
import UpdateRecord from "pages/records/updateRecord";
import CreateRecordTemplate from "pages/recordTemplates/createRecordTemplate";
import ListHistory from "pages/history/listHistory";
import { Navigate } from "react-router-dom";

const routes = [
  { route: "/record-templates", component: <ListRecordTemplate /> },
  { route: "/record-templates/record-template", component: <CreateRecordTemplate /> },
  { route: "/available-record-templates", component: <ChooseRecordTemplate /> },
  { route: "/records", component: <ListRecords /> },
  { route: "/record-templates/edit", component: <UpdateRecordTemplate /> },
  { route: "/records/record", component: <CreateRecord /> },
  { route: "/record/edit", component: <UpdateRecord /> },
  { route: "/history", component: <ListHistory /> },
  { route: "*", component: <Navigate to="/record-templates" replace /> },
];

export default routes;
