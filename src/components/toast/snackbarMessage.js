import React from "react";

const SnackbarMessage = ({ message }) => {
  return <div id="snackbar-success-message">{message}</div>;
};

export default SnackbarMessage;
