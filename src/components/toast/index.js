import { IconButton } from "@mui/material";
import { useSnackbar } from "notistack";
import { useEffect } from "react";
import CloseIcon from "@mui/icons-material/Close";

const useToastNotification = (message, variant, condition, autoHideDuration = 3000) => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const action = (key) => (
    <IconButton size="small" aria-label="close" onClick={() => closeSnackbar(key)}>
      <CloseIcon fontSize="small" color="#ffffff" />
    </IconButton>
  );

  useEffect(() => {
    if (condition) {
      enqueueSnackbar(message, {
        variant,
        autoHideDuration,
        action: action,
      });
    }
  }, [condition, message, variant, enqueueSnackbar, autoHideDuration]);
};

export default useToastNotification;
