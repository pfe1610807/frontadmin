import { Button, Typography } from "@mui/material";
import WithPermission from "components/permissions/withPermission";
import { useLocation, NavLink, useNavigate } from "react-router-dom";

const TcmsMenuItem = ({ index, link, title, onClick, isMenu, handleLinkClicked, isDropdown }) => {
  const MyMenu = WithPermission(
    ({ index, handleClick }) => (
      <Button key={index} onClick={handleClick} sx={{ width: "100%", padding: "14px" }}>
        <NavLink
          key={index}
          className={({ isActive }) =>
            `${isMenu ? "Dropdown-link" : "App-link"} ${
              isActive ? (isMenu ? "navactiveDropdown" : "navactiveApplink") : ""
            }`
          }
          to={link}
          onClick={handleLinkClicked}
        >
          <Typography component={"span"}>
            <div className="menuTabsLabel">{title}</div>
          </Typography>
        </NavLink>
      </Button>
    ),
    [`Maintenance_${title === "Record templates" ? "RecordTemplates" : title}_Menu_View`]
  );
  const location = useLocation();
  const navigate = useNavigate();

  const handleClick = (event) => {
    if (isDropdown) {
      onClick(event);
    } else {
      navigate(link);
    }
  };

  return (
    <div
      className={
        location.pathname.startsWith(link) ? "menuTabButton menuTabButtonSelected" : "menuTabButton"
      }
    >
      <MyMenu
        index={index}
        handleClick={handleClick}
        isMenu={isMenu}
        link={link}
        handleLinkClicked={handleLinkClicked}
        title={title}
      />
    </div>
  );
};

export default TcmsMenuItem;
