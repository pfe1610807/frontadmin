import { Menu, MenuItem } from "@mui/material";
import { useState } from "react";
import { Link } from "react-router-dom";
import TcmsMenuItem from "./TcmsMenuItem";

const PageDropdown = ({ index, title, link, options, isMenu, isDropdown, isDrawer }) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLinkClicked = (e) => {
    e.preventDefault();
  };

  return (
    <>
      <TcmsMenuItem
        key={index}
        index={index}
        link={link}
        title={title}
        onClick={handleClick}
        isMenu={isMenu}
        handleLinkClicked={handleLinkClicked}
        isDropdown={isDropdown}
      />
      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        sx={{ marginLeft: isDrawer ? "80px" : 0, top: isDrawer ? "40px" : "40px" }}
      >
        {options &&
          options.map((option, index) => (
            <MenuItem component={Link} key={index} to={link + option.link} onClick={handleClose}>
              <div className="Dropdown-link"> {option.title} </div>
            </MenuItem>
          ))}
      </Menu>
    </>
  );
};

export default PageDropdown;
