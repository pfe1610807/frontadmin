import React, { useState } from "react";
import { Grid, IconButton, Drawer } from "@mui/material";
import DensitySmallIcon from "@mui/icons-material/DensitySmall";
import TcmsMenuItem from "./TcmsMenuItem";
import PageDropdown from "./PageDropdown";
import { useSelector } from "react-redux";
import "assets/styles.css";

const FullMenu = ({ pages }) => {
  const [showDrawer, setShowDrawer] = useState(false);

  const toggleDrawer = () => {
    setShowDrawer(!showDrawer);
  };
  const { darkMode } = useSelector((state) => state.darkMode);
  return pages ? (
    <>
      <Grid
        item
        id="globalMenuTabs"
        className="navbar"
        justify="left"
        gap={2}
        sx={{
          flexGrow: 2,
          display: { xs: "none", md: "flex" },
          backgroundColor: darkMode ? "#292929" : "#42647b",
        }}
      >
        {pages.map((page, index) =>
          !page.isDropdown ? (
            <TcmsMenuItem
              key={index}
              title={page.title}
              link={page.link}
              isMenu={false}
              options={page.options}
              isDropdown={page.isDropdown}
              index={index}
            />
          ) : (
            <PageDropdown
              key={index}
              title={page.title}
              link={page.link}
              isMenu={false}
              options={page.options}
              isDropdown={page.isDropdown}
            />
          )
        )}
      </Grid>
      <Grid
        item
        xs="auto"
        sx={{
          display: { xs: "flex", md: "none" },
          justifyContent: "flex-end",
          alignItems: "center",
        }}
      >
        <IconButton color="inherit" onClick={toggleDrawer} className="icon">
          <DensitySmallIcon />
        </IconButton>

        <Drawer anchor="left" open={showDrawer} onClose={toggleDrawer}>
          <Grid
            container
            direction="column"
            justifyContent="flex-start"
            alignItems="stretch"
            sx={{ width: 100, backgroundColor: darkMode ? "#292929" : "#42647b" }}
          >
            {pages.map((page, index) =>
              !page.isDropdown ? (
                <TcmsMenuItem
                  key={index}
                  title={page.title}
                  link={page.link}
                  isMenu={false}
                  options={page.options}
                  isDropdown={page.isDropdown}
                  index={index}
                />
              ) : (
                <PageDropdown
                  key={index}
                  title={page.title}
                  link={page.link}
                  isMenu={false}
                  options={page.options}
                  isDrawer={true}
                  isDropdown={page.isDropdown}
                />
              )
            )}
          </Grid>
        </Drawer>
      </Grid>
    </>
  ) : null;
};

export default FullMenu;
