import { Typography } from "@mui/material";
import React, { useState, useEffect } from "react";

export const CurrentTime = () => {
  // var [date, setDate] = useState(new Date());
  const [date, setDate] = useState(new Date());
  useEffect(() => {
    var timer = setInterval(() => setDate(new Date()), 1000);
    return function cleanup() {
      clearInterval(timer);
    };
  });

  return (
    <Typography variant="h5" className="current_time">
      {date.toLocaleString([], {
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        hourCycle: "h23",
      })}
    </Typography>
  );
};

export default CurrentTime;
