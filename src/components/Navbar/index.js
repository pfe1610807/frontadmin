import React from "react";
import { AppBar, Grid } from "@mui/material";
import CurrentTime from "./CurrentTime";
import Configurator from "./lightAndDarkModeConfig";
import FullMenu from "./RoutesMenu/FullMenu";
import LoggedInUser from "./loggedInUser";
import { useSelector } from "react-redux";
import Notifications from "./notifications";

const Navbar = ({ darkMode, toggleDarkMode, isTrue }) => {
  const { token } = useSelector((state) => state.auth);

  const pages = [
    {
      title: "Record templates",
      link: "/record-templates",
      isDropdown: true,
      options: [
        { link: "", title: "List record templates" },
        { link: "/record-template", title: "Create record template" },
      ],
    },
    {
      title: "Records",
      link: "/records",
      isDropdown: false,
      options: [],
    },
    {
      title: "History",
      link: "/history",
      isDropdown: false,
      options: [],
    },
  ];

  return (
    <div id="globalHeader">
      <AppBar id="appHeader">
        <Grid container direction="row" alignItems="center" spacing={2}>
          {token ? (
            <>
              <Grid item>
                <FullMenu pages={pages} />
              </Grid>
              <Grid
                item
                container
                xs
                alignItems="center"
                justifyContent={isTrue ? "" : "flex-end"}
                spacing={2}
              >
                <Grid item>
                  <Configurator darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
                </Grid>
                <Grid item sx={{ fontWeight: "bold" }}>
                  <CurrentTime />
                </Grid>
                <Grid item>
                  <LoggedInUser />
                </Grid>
                <Grid item>
                  <Notifications />
                </Grid>
              </Grid>
            </>
          ) : (
            <Grid item container alignItems="center" justifyContent="center" spacing={2}>
              <Grid item>
                <Configurator darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
              </Grid>
              <Grid item sx={{ fontWeight: "bold", marginLeft: 2 }}>
                <CurrentTime />
              </Grid>
              <Grid item>
                <LoggedInUser />
              </Grid>
              <Grid item>
                <Notifications />
              </Grid>
            </Grid>
          )}
        </Grid>
      </AppBar>
    </div>
  );
};

export default Navbar;
