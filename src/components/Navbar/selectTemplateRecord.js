import React from "react";
import { IconButton, Tooltip } from "@mui/material";
import "assets/styles.css";
import { useNavigate } from "react-router-dom";
import WithPermission from "components/permissions/withPermission";
import TouchAppIcon from "@mui/icons-material/TouchApp";

function SelectTemplateRecord({ template, onItemSelect }) {
  const navigate = useNavigate();
  const handleChoose = async () => {
    if (template.id) {
      onItemSelect();
      navigate("/records/record", { state: { templateId: template.id } });
    } else {
      console.error("Template ID is undefined");
    }
  };

  return (
    <Tooltip title="Select">
      <IconButton aria-label="Select" onClick={handleChoose}>
        <TouchAppIcon className="action-MuiSvgIcon-root" />
      </IconButton>
    </Tooltip>
  );
}

export default WithPermission(SelectTemplateRecord, [
  "Maintenance_RecordTemplates_Action_Choose_View",
  "Maintenance_RecordTemplates_Action_Choose_Execute",
]);
