import { IconButton } from "@mui/material";
import NightlightOutlinedIcon from "@mui/icons-material/NightlightOutlined";
import LightModeIcon from "@mui/icons-material/LightMode";
import "assets/styles.css";

function Configurator({ darkMode, toggleDarkMode }) {
  return (
    <IconButton
      sx={{ ml: 1 }}
      onClick={toggleDarkMode}
      color="inherit"
      className="action-MuiSvgIcon-root"
    >
      {darkMode ? <NightlightOutlinedIcon /> : <LightModeIcon />}
    </IconButton>
  );
}

export default Configurator;
