import React, { useEffect, useState } from "react";
import {
  IconButton,
  Popover,
  List,
  ListItem,
  ListItemText,
  Typography,
  Badge,
  Box,
  ListItemButton,
  Stack,
} from "@mui/material";
import { useNotificationsByUser, markNotificationAsRead } from "api/notification_api";
import { useSelector } from "react-redux";
import { formatDistanceToNow } from "date-fns";
import JwtParser from "components/auth/JWTParser";
import { useRecordTemplateDetails } from "api/recordTemplateApi";
import PreviewRecordTemplate from "components/Navbar/previewRecordTemplate";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { markAllNotificationsAsRead } from "api/notification_api";

const Notifications = () => {
  const { socket } = useSelector((state) => state.socket);
  const { token } = useSelector((state) => state.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  const { data, error, isLoading, refetch } = useNotificationsByUser(decodedToken?.userId);
  const [openPreview, setOpenPreview] = useState(false);
  const [selectedNotification, setSelectedNotification] = useState(null);

  const { mutateAsync: markNotification } = markNotificationAsRead();
  const {
    data: recordTemplateDetails,
    isLoading: isDetailsLoading,
    refetch: refetchDetails,
  } = useRecordTemplateDetails(selectedNotification?.entityId);
  const { mutateAsync: readAll } = markAllNotificationsAsRead();
  useEffect(() => {
    if (socket) {
      socket.on("notification", (message) => {
        refetch();
      });
    }
  }, [socket]);

  const [anchorEl, setAnchorEl] = useState(null);
  const [visibleCount, setVisibleCount] = useState(5);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
    setVisibleCount(5);
  };

  const open = Boolean(anchorEl);
  const id = open ? "notifications-popover" : undefined;

  const numberUnreadNotifications = data?.filter((notification) => !notification.isRead).length;

  const sortedData = data?.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));

  const visibleNotifications = sortedData?.slice(0, visibleCount);

  const handleNotificationClick = async (item) => {
    await markNotification(item.id);
    refetch();
    setSelectedNotification(item);
    setOpenPreview(true);
    setAnchorEl(null);
    refetchDetails();
  };

  const handleMarkAllAsReadClick = () => {
    readAll(decodedToken?.userId, {
      onSuccess: () => {
        refetch();
      },
    });
  };

  return (
    <Box>
      <IconButton aria-describedby={id} onClick={handleClick}>
        <Badge badgeContent={numberUnreadNotifications} color="error">
          <NotificationsIcon />
        </Badge>
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        sx={{ marginTop: 1 }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
      >
        <Box width="320px" maxHeight="400px">
          <Box
            sx={{
              position: "fixed",
              backgroundColor: "rgba(255, 255, 255, 0.99)",
              boxShadow: "0 4px 4px -2px rgba(0, 0, 0, 0.25)",
              width: "inherit",
              zIndex: 1,
            }}
          >
            <Typography p={0.5} variant="h6">
              Notifications
            </Typography>
          </Box>
          <List
            sx={{
              overflowY: "auto",
              paddingTop: "10%",
            }}
          >
            {isLoading ? (
              <ListItem>
                <ListItemText primary="Loading..." />
              </ListItem>
            ) : error ? (
              <ListItem>
                <ListItemText primary="Error loading notifications" />
              </ListItem>
            ) : sortedData?.length === 0 ? (
              <ListItem>
                <ListItemText primary="No new notifications" />
              </ListItem>
            ) : (
              visibleNotifications?.map((item) => (
                <ListItemButton key={item.id} onClick={() => handleNotificationClick(item)}>
                  <ListItemText
                    primary={
                      <Typography
                        variant="body2"
                        color="textPrimary"
                        sx={{ fontWeight: item.isRead ? "normal" : "bold" }}
                      >
                        {item.message}
                      </Typography>
                    }
                    secondary={
                      <Typography variant="caption" color="textSecondary">
                        {formatDistanceToNow(new Date(item.createdAt), { addSuffix: true })}
                      </Typography>
                    }
                  />
                </ListItemButton>
              ))
            )}
          </List>
          <Stack
            direction="row"
            justifyContent="end"
            alignItems="center"
            spacing={2}
            sx={{
              height: "50px",
              position: "relative",
              // boxShadow: "0 -2px 4px rgba(0, 0, 0, 0.2)",
            }}
          >
            {sortedData?.length > visibleCount && (
              <Typography
                variant="caption"
                style={{ cursor: "pointer", color: "#3f51b5", fontWeight: "bold" }}
                onClick={() => setVisibleCount(visibleCount + 5)}
              >
                Show more (+{sortedData?.length - visibleCount})
              </Typography>
            )}
            <Typography
              variant="caption"
              style={{ cursor: "pointer", color: "#3f51b5", fontWeight: "bold" }}
              onClick={handleMarkAllAsReadClick}
            >
              Mark all as read
            </Typography>
          </Stack>
        </Box>
      </Popover>

      <PreviewRecordTemplate
        template={recordTemplateDetails}
        open={openPreview}
        setOpen={setOpenPreview}
        action="preview"
        handleOpen={() => setOpenPreview(true)}
      />
    </Box>
  );
};
export default Notifications;
