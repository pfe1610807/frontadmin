import React from "react";
import { Box, DialogContent, DialogTitle, Slide, Typography } from "@mui/material";
import "assets/styles.css";
import styled from "@emotion/styled";
import Dialog from "@mui/material/Dialog";
import SelectTemplateRecord from "./selectTemplateRecord";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function PreviewRecordTemplate({ template, open, setOpen, action }) {
  const handleClose = () => {
    setOpen(false);
  };

  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    "& .MuiDialogContent-root": {
      padding: theme.spacing(2),
    },
    "& .MuiDialogActions-root": {
      padding: theme.spacing(1),
    },
  }));

  return (
    <>
      {open === true && action === "preview" && (
        <BootstrapDialog
          id="preview-dialog"
          onClose={handleClose}
          aria-labelledby="preview-dialog-title"
          TransitionComponent={Transition}
          open={open}
          maxWidth="lg"
          fullWidth
        >
          <DialogTitle
            sx={{ m: 0, p: 2, display: "flex", alignItems: "center" }}
            id="preview-dialog-title"
          >
            Preview
          </DialogTitle>
          <DialogContent
            dividers
            sx={{
              maxHeight: "500px",
              overflow: "auto",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Box
              component="img"
              src={template?.image}
              alt="Record Template"
              width={"100%"}
              height={"100%"}
            />
            <Box>
              <SelectTemplateRecord template={template} onItemSelect={handleClose} />
            </Box>
          </DialogContent>
        </BootstrapDialog>
      )}
    </>
  );
}

export default PreviewRecordTemplate;
