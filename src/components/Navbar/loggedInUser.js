import React, { useState } from "react";
import { IconButton, ListItemIcon, Menu, MenuItem, Tooltip, Typography } from "@mui/material";
import PersonIcon from "@mui/icons-material/Person";
import "assets/styles.css";
import { Logout } from "@mui/icons-material";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { signOut } from "../../redux/authSlice";
import JwtParser from "components/auth/JWTParser";

function LoggedInUser() {
  const dispatch = useDispatch();
  const { token } = useSelector((state) => state.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    handleClose();
    dispatch(signOut());
  };

  return (
    <>
      <Tooltip title="Account settings">
        <IconButton
          sx={{ ml: 1 }}
          color="inherit"
          className="icon-button"
          onClick={handleClick}
          size="small"
          aria-controls={open ? "account-menu" : undefined}
          aria-haspopup="true"
          aria-expanded={open ? "true" : undefined}
        >
          <PersonIcon className="action-MuiSvgIcon-root" />
          <Typography variant="body2" paddingLeft={1}>
            {decodedToken?.sub}
          </Typography>
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        sx={{ top: "1%" }}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&::before": {
              content: '""',
              display: "block",
              position: "absolute",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "left", vertical: "top" }}
        anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
      >
        <MenuItem onClick={handleLogout}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </>
  );
}

export default LoggedInUser;
