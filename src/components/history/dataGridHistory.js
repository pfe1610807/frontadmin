import React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { TableContainer } from "@mui/material";

const DataGridHistory = ({ rows }) => {
  const columns = [
    { field: "user", headerName: "USER", flex: 1 },
    { field: "entity", headerName: "ENTITY", flex: 1 },
    {
      field: "action",
      headerName: "ACTION",
      flex: 1,
      renderCell: (params) => params.value,
    },
    {
      field: "timestamps",
      headerName: "DATE",
      width: 200,
      renderCell: (params) => (
        <div>
          <div>{params.row.date}</div>
          <div>{params.row.time}</div>
        </div>
      ),
    },
  ];

  return (
    <TableContainer style={{ height: 525, overflow: "auto" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 8 },
          },
        }}
        pageSizeOptions={[8, 10]}
      />
    </TableContainer>
  );
};

export default DataGridHistory;
