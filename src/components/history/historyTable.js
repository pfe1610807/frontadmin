import React, { useEffect, useRef, useState } from "react";
import ErrorAlert from "components/alert/ErrorAlert";
import { useRecord } from "api/apiRecord_Answers";
import "assets/styles.css";
import DataGridHistory from "./dataGridHistory";
import TableSkeleton from "pages/records/skeletonrecord/tableRecordSkeleton.js";
import { useUsers } from "api/userApi";
import StatusIcon from "components/record/actions/statusIcon";
import FilterComponent from "components/filters/filterListRecord";
import ErrorContent from "components/actionsData/errorContent";
import NoContent from "components/actionsData/noContent";
import AssignmentOutlinedIcon from "@mui/icons-material/AssignmentOutlined";
import AssignmentTurnedInOutlinedIcon from "@mui/icons-material/AssignmentTurnedInOutlined";
import dayjs from "dayjs";
function HistoryTable() {
  const dataGridRef = useRef(null);
  const [selectedDateTime, setSelectedDateTime] = useState(null);
  const [history, setHistory] = useState([]);
  const [filteredHistory, setFilteredHistory] = useState([]);
  const [users, setUsers] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const [textFilter, setTextFilter] = useState("");
  const [actionFilter, setActionFilter] = useState("ALL");
  const [alertMessage, setAlertMessage] = useState("");
  const { data, error, isLoading } = useRecord();
  const { data: userData } = useUsers();

  // Fetching Data Of history
  // useEffect(() => {
  //   if (data) {
  //     setHistory(data);
  //     setFilteredHistory(data);
  //   }
  // }, [data]);
  useEffect(() => {
    if (data) {
      const sortedData = [...data].sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt));
      setHistory(sortedData);
      setFilteredHistory(sortedData);
    }
  }, [data]);
  useEffect(() => {
    if (userData) {
      setUsers(userData);
    }
  }, [userData]);

  const extractDate = (dateObject) => {
    return dayjs(dateObject.$d).format("YYYY-MM-DD");
  };

  const getDate = (date) => {
    if (!date) return null;
    return date.substring(0, 10);
  };

  const getTime = (date) => {
    if (!date) return null;
    return date.substring(11, 16);
  };

  const getAction = (action) => {
    return action === "Create" ? (
      <StatusIcon className="sign-button">
        <AssignmentOutlinedIcon fontSize="small" className="sign-button-icon" />
        <span style={{ marginLeft: "8px" }}>Create</span>
      </StatusIcon>
    ) : (
      <StatusIcon className="sign-history-button">
        <AssignmentTurnedInOutlinedIcon fontSize="small" className="open-history-button-icon" />
        <span style={{ marginLeft: "8px" }}>Sign</span>
      </StatusIcon>
    );
  };

  const rows = history.flatMap((record) => {
    const user = users?.find((user) => user.userId === record.createdBy);
    console.log(record);
    const createDate = getDate(record.createdAt);
    const createTime = getTime(record.createdAt);
    const signDate = getDate(record.signedAt);
    const signTime = getTime(record.signedAt);

    return signDate
      ? [
          {
            id: `${record.id}-create`,
            user: user?.username,
            entity: "Record",
            action: getAction("Create"),
            date: createDate,
            time: createTime,
          },
          {
            id: `${record.id}-sign`,
            user: user?.username,
            entity: "Record",
            action: getAction("Sign"),
            date: signDate,
            time: signTime,
          },
        ]
      : [
          {
            id: `${record.id}-create`,
            user: user?.username,
            entity: "Record",
            action: getAction("Create"),
            date: createDate,
            time: createTime,
          },
        ];
  });

  useEffect(() => {
    const filterHistory = () => {
      let filtered = rows;

      // Filter by text
      if (textFilter.trim() !== "") {
        filtered = filtered?.filter((record) => {
          const filterText = textFilter.toLowerCase();
          return (
            record.user?.toLowerCase().includes(filterText) ||
            record.entity?.toLowerCase().includes(filterText) ||
            record.date?.toLowerCase().includes(filterText) ||
            record.time?.toLowerCase().includes(filterText)
          );
        });
      }
      // Filter by status
      if (actionFilter !== "ALL") {
        filtered = filtered.filter((record) => {
          console.log("record", record);
          return actionFilter === "Create"
            ? record.action.props?.children[1]?.props?.children == "Create"
            : record.action.props?.children[1]?.props?.children == "Sign";
          // return statusFilter === "OPEN" ? record.isOpen : !record.isOpen;
        });
      }
      // Filter by selected date time
      if (selectedDateTime) {
        filtered = filtered?.filter((record) => {
          const selectedDate = extractDate(selectedDateTime);
          const timestamps = record.date;
          return selectedDate === timestamps;
        });
      }
      setFilteredHistory(filtered);
    };

    filterHistory();
  }, [textFilter, actionFilter, selectedDateTime, history]);

  const handleTextFilterChange = (event) => {
    setTextFilter(event.target.value);
  };

  const handleActionFilterChange = (event) => {
    setActionFilter(event.target.value);
  };

  const handleDateTimeChange = (newDateTime) => {
    setSelectedDateTime(newDateTime);
  };

  return (
    <>
      {alertMessage && <ErrorAlert message={alertMessage} />}
      <FilterComponent
        textFilter={textFilter}
        statusFilter={actionFilter}
        handleTextFilterChange={handleTextFilterChange}
        handleStatusFilterChange={handleActionFilterChange}
        selectedDateTime={selectedDateTime}
        handleDateTimeChange={handleDateTimeChange}
      />
      {isLoading ? (
        <TableSkeleton />
      ) : error ? (
        <ErrorContent />
      ) : data?.length === 0 ? (
        <NoContent
          title="No history available"
          body="Currently, there are no actions available to display."
        />
      ) : (
        <DataGridHistory rows={filteredHistory} anchorEl={anchorEl} ref={dataGridRef} />
      )}
    </>
  );
}

export default HistoryTable;
