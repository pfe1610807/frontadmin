import { Box, Grid, Typography } from "@mui/material";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import React from "react";

const ErrorContent = () => (
  <Grid item xs={12}>
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "50vh",
        textAlign: "center",
        padding: "2rem",
      }}
    >
      <ErrorOutlineIcon color="error" sx={{ fontSize: 40, marginBottom: "0.5rem" }} />
      <Typography variant="h5">Something went wrong</Typography>
      <Typography variant="body1">
        There was a problem processing the request. Please try again.
      </Typography>
    </Box>
  </Grid>
);
export default ErrorContent;
