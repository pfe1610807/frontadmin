import { Box, Grid, Typography } from "@mui/material";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import React from "react";
const NoRecordContent = ({ title, body }) => (
  <Grid item xs={12}>
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "50vh",
        textAlign: "center",
        padding: "2rem",
      }}
    >
      <LibraryBooksIcon color="action" sx={{ fontSize: 40, marginBottom: "0.5rem" }} />
      <Typography variant="h5"> {title}</Typography>
      <Typography variant="body1">{body}</Typography>
    </Box>
  </Grid>
);
export default NoRecordContent;
