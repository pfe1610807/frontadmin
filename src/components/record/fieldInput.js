import React, { useState, useEffect } from "react";
import { Typography, FormGroup, Stack, Grid } from "@mui/material";
import DateTimePickerComponent from "components/field/fieldType/dateTimePickerComponent";
import CheckboxComponent from "components/field/fieldType/checkboxComponent";
import OneLineTextComponent from "components/field/fieldType/oneLineTextComponent";
import DropdownComponent from "components/field/fieldType/dropdownComponent";
import MultiChoiceComponent from "components/field/fieldType/multiChoiceComponent";
import MultipleLineComponent from "components/field/fieldType/multipleLineComponent";
function FieldInput({ field, handleAnswerChange, response }) {
  const [selectedOption, setSelectedOption] = useState(
    field.fieldType === "multichoice" ? response : ""
  );

  const handleChange = (event) => {
    setSelectedOption(event.target.value);
    handleAnswerChange(field.id, event.target.value);
  };

  useEffect(() => {
    if (field.fieldType === "multichoice") {
      setSelectedOption(response);
    }
  }, [response, field.fieldType]);

  return (
    <Grid>
      <Stack direction="row" spacing={2}>
        <Typography variant="body1" sx={{ marginTop: "20px", left: "20px" }}>
          {field.fieldName}
          {field.isMandatory && <span style={{ color: "red" }}> * </span>}
        </Typography>
      </Stack>
      {field.fieldType === "text" ? (
        <OneLineTextComponent
          isAdminComponent={false}
          handleAnswerChange={handleAnswerChange}
          response={response}
          field={field}
        />
      ) : field.fieldType === "Dropdown" ? (
        <DropdownComponent
          isAdminComponent={false}
          handleAnswerChange={handleAnswerChange}
          response={response}
          field={field}
          handleChange={handleChange}
        />
      ) : field.fieldType === "MutlipleLine" ? (
        <MultipleLineComponent
          isAdminComponent={false}
          handleAnswerChange={handleAnswerChange}
          response={response}
          field={field}
        />
      ) : field.fieldType === "datetime" ? (
        <DateTimePickerComponent
          isAdminComponent={false}
          handleAnswerChange={handleAnswerChange}
          response={response}
          field={field}
        />
      ) : field.fieldType === "checkbox" ? (
        <CheckboxComponent
          isAdminComponent={false}
          handleAnswerChange={handleAnswerChange}
          response={response}
          field={field}
        />
      ) : field.fieldType === "multichoice" ? (
        <MultiChoiceComponent
          isAdminComponent={false}
          handleAnswerChange={handleAnswerChange}
          response={response}
          field={field}
          handleChange={handleChange}
          selectedOption={selectedOption}
        />
      ) : null}
    </Grid>
  );
}

export default FieldInput;
