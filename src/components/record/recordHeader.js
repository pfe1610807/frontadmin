import React from "react";
import { Typography, Card, CardContent, Button } from "@mui/material";

function RecordHeader({ recordTitle, recordDescription }) {
  return (
    <Card>
      <CardContent>
        <Typography variant="body1" style={{ fontWeight: "bold" }}>
          {recordTitle}
        </Typography>
        <Typography variant="h6" style={{ fontWeight: "normal" }}>
          {recordDescription}
        </Typography>
      </CardContent>
    </Card>
  );
}

export default RecordHeader;
