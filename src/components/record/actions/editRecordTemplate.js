import React from "react";
import { IconButton, Tooltip } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import { useNavigate } from "react-router-dom";
import WithPermission from "components/permissions/withPermission";
import { motion } from "framer-motion";

const EditRecordTemplate = ({ record, openRecords, disabled }) => {
  const navigate = useNavigate();

  const handleEditClick = (recordId) => {
    if (!openRecords.find((record) => record.id === recordId).isOpen) {
      console.error("Record is already signed and cannot be edited");
      return;
    }
    const record = openRecords.find((record) => record.id === recordId);
    if (record && record.recordTemplate && record.recordTemplate.id) {
      const recordTemplateId = record.recordTemplate.id;
      console.log("recordTemplateId", recordTemplateId);
      navigate("/record/edit", {
        state: {
          recordId: recordId,
          recordTemplateId: recordTemplateId,
        },
      });
    } else {
      console.error("Record or recordTemplate not found for recordId:", recordId);
    }
  };
  const ButtonDisable = !record.isOpen || disabled;

  return (
    <>
      {!record.isOpen || disabled ? (
        <motion.div
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: 0.6 }}
        >
          <IconButton
            id="iconButton_edit_Record_Answers"
            className={`action-MuiSvgIcon-root ${record.isOpen ? "" : "disabled-button"}`}
            aria-label="edit "
            disabled={ButtonDisable}
            onClick={() => handleEditClick(record.id)}
          >
            <EditIcon />
          </IconButton>
        </motion.div>
      ) : (
        <Tooltip title="Edit">
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: 0.6 }}
          >
            <IconButton
              id="iconButton_edit_Record_Answers"
              aria-label="edit"
              onClick={() => handleEditClick(record.id)}
              className={`action-MuiSvgIcon-root ${!ButtonDisable ? "" : "disabled-button"}`}
              disabled={ButtonDisable}
            >
              <EditIcon />
            </IconButton>
          </motion.div>
        </Tooltip>
      )}
    </>
  );
};

export default WithPermission(EditRecordTemplate, [
  "Maintenance_Records_Action_Edit_View",
  "Maintenance_Records_Action_Edit_Execute",
]);
