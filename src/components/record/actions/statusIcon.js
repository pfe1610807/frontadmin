import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
const StatusIcon = styled(Box)(({ color }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  padding: "4px 8px",
  borderRadius: "12px",
  border: `2px solid ${color}`,
  color: color,
}));

export default StatusIcon;
