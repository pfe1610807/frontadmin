import React from "react";
import { IconButton, Tooltip } from "@mui/material";
import GetAppIcon from "@mui/icons-material/GetApp";
import WithPermission from "components/permissions/withPermission";
import { motion } from "framer-motion";
import { useExportRecordTemplatePdf } from "api/apiRecord_Answers";
import { useSnackbar } from "notistack";
import SnackbarMessage from "components/toast/snackbarMessage";

const ExportPdfRecordTemplate = ({ record, disabled }) => {
  const { enqueueSnackbar } = useSnackbar();
  const { mutate: exportPdf } = useExportRecordTemplatePdf();
  console.log("record12:", record);
  const handleExportClick = (record) => {
    exportPdf(record, {
      onSuccess: () => {
        // enqueueSnackbar("PDF exported successfully", { variant: "success" });
        enqueueSnackbar(<SnackbarMessage message="PDF exported successfully" />, {
          variant: "success",
        });
      },
      onError: () => {
        enqueueSnackbar("Failed to export PDF", { variant: "error" });
      },
    });
  };
  return (
    <>
      {record.isOpen || disabled ? (
        <motion.div
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: 1 }}
        >
          <IconButton
            className={`action-MuiSvgIcon-root ${!record.isOpen ? "" : "disabled-button"}`}
            aria-label="export-pdf"
            id="export-pdf-button"
            disabled={record.isOpen || disabled}
            onClick={() => handleExportClick(record)}
          >
            <GetAppIcon />
          </IconButton>
        </motion.div>
      ) : (
        <Tooltip title="Export PDF">
          <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            transition={{ delay: 1 }}
          >
            <IconButton
              aria-label="export-pdf"
              id="export-pdf-button"
              onClick={() => handleExportClick(record)}
              className={`action-MuiSvgIcon-root ${!record.isOpen ? "" : "disabled-button"}`}
            >
              <GetAppIcon />
            </IconButton>
          </motion.div>
        </Tooltip>
      )}
    </>
  );
};

export default WithPermission(ExportPdfRecordTemplate, [
  "Maintenance_Records_Action_Export_View",
  "Maintenance_Records_Action_Export_Execute",
]);
