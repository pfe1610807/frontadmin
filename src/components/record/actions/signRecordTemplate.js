import React, { useState } from "react";
import { IconButton, Tooltip } from "@mui/material";
import DoneAllIcon from "@mui/icons-material/DoneAll";
import WithPermission from "components/permissions/withPermission";
import { motion } from "framer-motion";
import { useSnackbar } from "notistack";
import useToastNotification from "../../toast/index";
import { useSigneRecord } from "api/apiRecord_Answers";
import SnackbarMessage from "components/toast/snackbarMessage";

function SignRecordTemplate({ record, openRecords, setOpenRecords, disabled }) {
  const [error, setError] = useState("");
  const isSigned = !!record.signedAt;
  const { enqueueSnackbar } = useSnackbar();
  const SigneRecordMutation = useSigneRecord();

  const areMandatoryFieldsFilled = (record) => {
    const mandatoryFields = record.recordTemplate.fields.filter((field) => field.isMandatory);

    for (const field of mandatoryFields) {
      const answer = record.answers.find((ans) => ans.field.id === field.id);
      if (!answer || !answer.answerText) {
        return false;
      }
    }

    return true;
  };

  const handleSignClick = async (record) => {
    if (!areMandatoryFieldsFilled(record)) {
      setError("Mandatory fields are not filled");
      return;
    }
    const currentTimeUTC = new Date().toISOString();
    const currentTimeUTCPlus1 = new Date(
      new Date(currentTimeUTC).getTime() + 60 * 60 * 1000
    ).toISOString();
    SigneRecordMutation.mutate(record.id, {
      onSuccess: () => {
        const updatedRecords = openRecords.map((rec) =>
          rec.id === record.id ? { ...rec, isOpen: false, signedAt: currentTimeUTCPlus1 } : rec
        );
        setOpenRecords(updatedRecords);
        enqueueSnackbar(<SnackbarMessage message="Record signed successfully" />, {
          variant: "success",
        });
      },
      onError: (error) => {
        setError("An error occurred while signing the record.");
        enqueueSnackbar(<SnackbarMessage message="Record not signed successfully" />, {
          variant: "error",
        });
      },
    });
  };
  useToastNotification(error, "error", !!error);

  return (
    <Tooltip title={isSigned ? "Template already signed" : "Sign"}>
      <span>
        <motion.div
          initial={{ opacity: 0, y: 20 }}
          animate={{ opacity: 1, y: 0 }}
          transition={{ delay: 0.8 }}
        >
          <IconButton
            id="signButton"
            onClick={() => handleSignClick(record)}
            disabled={isSigned || disabled}
            className={`action-MuiSvgIcon-root ${isSigned ? "disabled-button" : ""}`}
          >
            <DoneAllIcon />
          </IconButton>
        </motion.div>
      </span>
    </Tooltip>
  );
}

export default WithPermission(SignRecordTemplate, [
  "Maintenance_Records_Action_Sign_View",
  "Maintenance_Records_Action_Sign_Execute",
]);
