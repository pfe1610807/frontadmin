import React, { useEffect, useRef, useState } from "react";
import SignRecordTemplate from "./actions/signRecordTemplate";
import ExportPdfRecordTemplate from "./actions/exportPdfRecordTemplate";
import EditRecordTemplate from "./actions/editRecordTemplate";
import OpenIcon from "@mui/icons-material/LockOpen";
import SignedIcon from "@mui/icons-material/Lock";
import Tooltip from "@mui/material/Tooltip";
import StatusIcon from "./actions/statusIcon";
import DataGridComponent from "./dataGridRecord";
import "assets/styles.css";
import { useSelector } from "react-redux";
import TableSkeleton from "pages/records/skeletonrecord/tableRecordSkeleton.js";
import ErrorContent from "../actionsData/errorContent";
import NoContent from "../actionsData/noContent";
import FilterComponent from "components/filters/filterListRecord";
import JwtParser from "components/auth/JWTParser";
import { useRecordByUserId } from "api/apiRecord_Answers";
function RecordTable() {
  const dataGridRef = useRef(null);
  const [selectedDateTime, setSelectedDateTime] = useState(null);
  const [openRecords, setOpenRecords] = useState([]);
  const [filteredRecords, setFilteredRecords] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const [textFilter, setTextFilter] = useState("");
  const [statusFilter, setStatusFilter] = useState("ALL");
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);
  const { data, error, isLoading } = useRecordByUserId(decodedToken?.userId);
  //Fetching Data Of user
  useEffect(() => {
    if (data) {
      const sortedData = [...data].sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt));
      setOpenRecords(sortedData);
      setFilteredRecords(data);
    }
  }, [data]);
  // Function to filter records
  useEffect(() => {
    const filterRecords = () => {
      let filtered = openRecords;
      // Filter by text

      if (textFilter.trim() !== "") {
        filtered = filtered.filter((record) => {
          const template = record.recordTemplate;
          const filterText = textFilter.toLowerCase();
          return (
            template.title.toLowerCase().includes(filterText) ||
            template.description.toLowerCase().includes(filterText) ||
            (record.createdAt && record.createdAt.includes(filterText)) ||
            (record.signedAt &&
              record.signedAt !== "Not signed yet" &&
              record.signedAt.toLowerCase().includes(filterText))
          );
        });
      }
      // Filter by status
      if (statusFilter !== "ALL") {
        filtered = filtered.filter((record) => {
          return statusFilter === "OPEN" ? record.isOpen : !record.isOpen;
        });
      }
      // Filter by selected date time
      if (selectedDateTime) {
        filtered = filtered.filter((record) => {
          if (!record.isOpen && record.signedAt) {
            const selectedDate = new Date(selectedDateTime).toDateString();
            const signedDate = new Date(record.signedAt).toDateString();
            return selectedDate === signedDate;
          } else if (record.isOpen && record.createdAt) {
            const selectedDate = new Date(selectedDateTime).toDateString();
            const createdDate = new Date(record.createdAt).toDateString();
            return selectedDate === createdDate;
          }
          return false;
        });
      }

      setFilteredRecords(filtered);
    };

    filterRecords();
  }, [openRecords, textFilter, statusFilter, selectedDateTime]);
  const handleTextFilterChange = (event) => {
    setTextFilter(event.target.value);
  };

  const handleStatusFilterChange = (event) => {
    setStatusFilter(event.target.value);
  };

  const handleDateTimeChange = (newDateTime) => {
    setSelectedDateTime(newDateTime);
  };
  if (error) {
    return <ErrorContent />;
  }

  const getDate = (date) => {
    if (!date) return null;
    return date.substring(0, 10);
  };
  const getTime = (date) => {
    if (!date) return null;
    return date.substring(11, 16);
  };

  const getStatus = (isOpen) => {
    return isOpen ? (
      <Tooltip title="Open">
        <StatusIcon className="open-button">
          <OpenIcon fontSize="small" className="open-button-icon" />
          <span style={{ marginLeft: "8px" }}>Open</span>
        </StatusIcon>
      </Tooltip>
    ) : (
      <Tooltip title="Signed">
        <StatusIcon className="sign-button">
          <SignedIcon fontSize="small" className="sign-button-icon" />
          <span style={{ marginLeft: "8px" }}>Signed</span>
        </StatusIcon>
      </Tooltip>
    );
  };

  const rows = filteredRecords.map((record) => ({
    id: record.id,
    title: record.recordTemplate.title,
    description: record.recordTemplate.description,
    createDate: getDate(record.createdAt),
    createTime: getTime(record.createdAt),
    signDate: record.signedAt ? getDate(record.signedAt) : "Not signed yet",
    signTime: record.signedAt ? getTime(record.signedAt) : "",
    status: getStatus(record.isOpen),

    actions: (
      <>
        <EditRecordTemplate record={record} openRecords={openRecords} />

        <SignRecordTemplate
          record={record}
          openRecords={openRecords}
          setOpenRecords={setOpenRecords}
        />

        <ExportPdfRecordTemplate record={record} />
      </>
    ),
  }));

  return (
    <>
      <FilterComponent
        textFilter={textFilter}
        statusFilter={statusFilter}
        handleTextFilterChange={handleTextFilterChange}
        handleStatusFilterChange={handleStatusFilterChange}
        selectedDateTime={selectedDateTime}
        isRecord={true}
        handleDateTimeChange={handleDateTimeChange}
      />
      {isLoading ? (
        <TableSkeleton />
      ) : filteredRecords.length === 0 ? (
        <NoContent
          title="No record available"
          body="Currently, there are no record  available to display."
        />
      ) : (
        <DataGridComponent rows={rows} anchorEl={anchorEl} ref={dataGridRef} />
      )}
    </>
  );
}

export default RecordTable;
