import React from "react";
import { Dialog, DialogActions, DialogContent, DialogContentText, Button } from "@mui/material";

const ConfirmationDialog = ({ open, message, onConfirm, onCancel }) => {
  return (
    <Dialog open={open}>
      <DialogContent>
        <DialogContentText>{message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onCancel} color="primary">
          Cancel
        </Button>
        <Button onClick={onConfirm} color="primary" autoFocus>
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;
