// DataGridComponent.js
import React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { TableContainer } from "@mui/material";

const DataGridComponent = ({ rows }) => {
  const columns = [
    { field: "title", headerName: "TITLE", flex: 1 },
    { field: "description", headerName: "DESCRIPTION", flex: 1 },
    {
      field: "status",
      headerName: "Status",
      flex: 1,
      renderCell: (params) => params.value,
    },
    {
      field: "CreatedDate",
      headerName: "CREATED AT",
      flex: 1,
      renderCell: (params) => (
        <div>
          <div>{params.row.createDate}</div>
          <div>{params.row.createTime}</div>
        </div>
      ),
    },

    {
      field: "SignedDate",
      headerName: "SIGNED AT",
      flex: 1,
      renderCell: (params) => (
        <div>
          <div>{params.row.signDate}</div>
          <div>{params.row.signTime}</div>
        </div>
      ),
    },
    {
      field: "actions",
      headerName: "ACTIONS",
      flex: 1,
      renderCell: (params) => {
        return rows.find((row) => row.id === params.row.id)?.actions || null;
      },
    },
  ];
  return (
    <TableContainer style={{ height: 500, overflow: "auto" }}>
      <DataGrid
        rows={rows}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 8 },
          },
        }}
        pageSizeOptions={[8, 10]}
      />
    </TableContainer>
  );
};

export default DataGridComponent;
