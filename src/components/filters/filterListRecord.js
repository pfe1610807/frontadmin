// FilterComponent.js
import React from "react";
import { TextField, FormControl, InputLabel, Select, MenuItem, Grid } from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";

const FilterComponent = ({
  textFilter,
  statusFilter,
  handleTextFilterChange,
  handleStatusFilterChange,
  selectedDateTime,
  handleDateTimeChange,
  isRecord,
}) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={3} sx={{ marginBottom: "30px" }}>
          <TextField
            label="Title Filter"
            variant="outlined"
            value={textFilter}
            onChange={handleTextFilterChange}
            fullWidth
          />
        </Grid>
        <Grid item xs={12} md={3} sx={{ marginBottom: "30px" }}>
          <FormControl variant="outlined" fullWidth>
            <InputLabel>Status Filter</InputLabel>
            <Select
              value={statusFilter}
              onChange={handleStatusFilterChange}
              label="Status Filter"
              fullWidth
              style={{ minHeight: "45px" }}
            >
              <MenuItem value="ALL">All</MenuItem>
              {isRecord
                ? [
                    <MenuItem key="OPEN" value="OPEN">
                      Open
                    </MenuItem>,
                    <MenuItem key="SIGNED" value="SIGNED">
                      Signed
                    </MenuItem>,
                  ]
                : [
                    <MenuItem key="Create" value="Create">
                      Create
                    </MenuItem>,
                    <MenuItem key="Sign" value="Sign">
                      Sign
                    </MenuItem>,
                  ]}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12} md={3} sx={{ marginBottom: "30px" }}>
          <DatePicker
            sx={{ width: "100%" }}
            label="Date Time Picker"
            value={selectedDateTime}
            onChange={handleDateTimeChange}
            renderInput={(params) => <TextField {...params} variant="outlined" />}
            fullWidth
            componentsProps={{
              actionBar: {
                actions: ["clear"],
              },
            }}
          />
        </Grid>
      </Grid>
    </LocalizationProvider>
  );
};

export default FilterComponent;
