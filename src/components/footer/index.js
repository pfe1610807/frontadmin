import React from "react";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import BottomNavigation from "@mui/material/BottomNavigation";
import Paper from "@mui/material/Paper";
import { Button } from "@mui/material";

export default function Footer({ action, handleOnSubmit }) {
  return (
    <>
      <Box sx={{ pb: 7 }}>
        <CssBaseline />
        <Paper sx={{ position: "fixed", bottom: 0, left: 0, right: 0 }} elevation={3}>
          <BottomNavigation>
            <Box
              sx={{
                display: "flex",
                justifyContent: "flex-end",
                paddingRight: "70px",
                paddingBottom: "0.5%",
                flexGrow: 1,
              }}
            >
              <Button variant="contained" className="submitButton" onClick={handleOnSubmit}>
                {action}
              </Button>
            </Box>
          </BottomNavigation>
        </Paper>
      </Box>
    </>
  );
}
