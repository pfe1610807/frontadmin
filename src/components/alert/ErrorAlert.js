import * as React from "react";
import Alert from "@mui/material/Alert";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";

export default function ErrorAlert({ message }) {
  const [showAlert, setShowAlert] = React.useState(true);

  React.useEffect(() => {
    const timeout = setTimeout(() => {
      setShowAlert(false);
    }, 2000);

    return () => clearTimeout(timeout);
  }, []);

  return (
    showAlert && (
      <Alert
        severity="error"
        sx={{
          position: "fixed",
          bottom: "20px",
          left: "50%",
          transform: "translateX(-50%)",
          borderRadius: "8px",
          boxShadow: 4,
          zIndex: 9999,
          background: "red",
        }}
        icon={<ErrorOutlineIcon fontSize="inherit" color="error" />}
      >
        {message}
      </Alert>
    )
  );
}
