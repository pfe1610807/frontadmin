import * as React from "react";
import Alert from "@mui/material/Alert";
import CheckIcon from "@mui/icons-material/Check";

export default function SuccessAlert({ message }) {
  return (
    <Alert
      id="successMessageId"
      className="customAlert"
      icon={<CheckIcon fontSize="inherit" />}
      severity="success"
    >
      {message}
    </Alert>
  );
}
// import React, { useEffect, useState } from "react";
// import Alert from "@mui/material/Alert";
// import AlertTitle from "@mui/material/AlertTitle";
// import "assets/styles.css";

// const SuccessAlert = ({ message }) => {
//   return (
//     <Alert variant="filled" severity="success" id="successMessageId" className="customAlert">
//       <AlertTitle>Success</AlertTitle>
//       {message}
//     </Alert>
//   );
// };

// export default SuccessAlert;
