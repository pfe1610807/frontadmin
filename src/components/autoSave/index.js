import React, { useEffect, useState } from "react";
import { Typography } from "@mui/material";
import { useUpdateAnswer } from "api/apiRecord_Answers";

function AutoSave({ fieldResponses, isSaving, setIsSaving, data }) {
  // whenever an answer change occurs, update it
  const updateAnswerMutation = useUpdateAnswer();
  useEffect(() => {
    const saveData = async () => {
      if (isSaving === true) {
        // Use a simple for loop to handle each update synchronously
        for (const answer of data || []) {
          const answerId = answer.id;
          const answerText = fieldResponses[answer.field.id];
          await updateAnswerMutation.mutateAsync({ answerId, answerText });
        }

        // After all updates are done, set isSaving to false
        setIsSaving(false);

        // Set a timeout for UI purposes
        const timeoutId = setTimeout(() => {
          setIsSaving(); // Update the UI
        }, 1500);

        // Clean up the timeout when the component unmounts or dependencies change
        return () => clearTimeout(timeoutId);
      }
    };

    saveData();
  }, [isSaving, fieldResponses]);

  useEffect(() => {
    console.log("fieldResponses", fieldResponses);
    // save after a delay of 500ms
    const timeout = setTimeout(() => {
      setIsSaving(true);
    }, 500);
    return () => clearTimeout(timeout);
  }, [fieldResponses]);

  // solves the problem of displaying (saving, saved..) when we first open the page
  const [count, setCount] = useState(0);
  useEffect(() => {
    setCount(count + 1);
  }, [isSaving]);

  return (
    <>
      {count >= 4 && isSaving === true && <Typography variant="body2">Saving...</Typography>}
      {count >= 4 && isSaving === false && <Typography variant="body2">Saved.</Typography>}
    </>
  );
}

export default AutoSave;
