import React from "react";
import { useSelector } from "react-redux";
import { useUser } from "api/userApi";
import JwtParser from "components/auth/JWTParser";

const WithPermission = (Component, permissions) => {
  return function WrappedComponent(props) {
    const { token } = useSelector((state) => state?.auth);
    const decodedToken = JwtParser.parseJwtToken(token?.token);
    const { data } = useUser(decodedToken?.userId);

    const userPermissions = data?.roles?.flatMap((role) =>
      role.permissions.map((permission) => permission.permissionName)
    );

    const hasPermission = (type) =>
      permissions.some(
        (permission) => userPermissions?.includes(permission) && permission.endsWith(type)
      );

    const hasViewPermission = hasPermission("View");
    const hasExecutePermission = hasPermission("Execute");

    if (hasViewPermission) {
      return <Component {...props} disabled={!hasExecutePermission} />;
    } else {
      return null;
    }
  };
};

export default WithPermission;
