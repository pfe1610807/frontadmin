import React from "react";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { Box, DialogContent, DialogTitle, IconButton, Slide, Tooltip } from "@mui/material";
import "assets/styles.css";
import CloseIcon from "@mui/icons-material/Close";
import styled from "@emotion/styled";
import Dialog from "@mui/material/Dialog";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
function PreviewRecordTemplate({ template, open, setOpen, action, handleOpen }) {
  const handleClose = () => {
    setOpen(false);
  };
  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    "& .MuiDialogContent-root": {
      padding: theme.spacing(2),
    },
    "& .MuiDialogActions-root": {
      padding: theme.spacing(1),
    },
  }));
  return (
    <>
      <Tooltip title="Preview">
        <IconButton
          id="preview-template"
          aria-label="preview"
          onClick={() => handleOpen("preview")}
          className="action-MuiSvgIcon-root"
        >
          <VisibilityIcon fontSize="small" />
        </IconButton>
      </Tooltip>
      {open === true && action === "preview" && (
        <BootstrapDialog
          id="preview-dialog"
          onClose={handleClose}
          aria-labelledby="preview-dialog-title"
          TransitionComponent={Transition}
          open={open}
          maxWidth="lg"
          fullWidth
        >
          <DialogTitle sx={{ m: 0, p: 2 }} id="preview-dialog-title">
            Preview
          </DialogTitle>
          <IconButton
            id="preview-close"
            aria-label="close"
            onClick={handleClose}
            sx={{
              position: "absolute",
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            <CloseIcon />
          </IconButton>
          <DialogContent
            dividers
            sx={{
              maxHeight: "500px",
              overflow: "auto",
            }}
          >
            <Box
              component="img"
              src={template.image}
              alt="Record Template"
              width={"100%"}
              height={"100%"}
            />
          </DialogContent>
        </BootstrapDialog>
      )}
    </>
  );
}

export default PreviewRecordTemplate;
