import React from "react";
import { IconButton, Tooltip } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import ActionConfirm from "./actionConfirm";
import "assets/styles.css";
import { useDeleteRecordTemplate } from "api/recordTemplateApi";
import WithPermission from "components/permissions/withPermission";
import { useSnackbar } from "notistack";
import SnackbarMessage from "components/toast/snackbarMessage";

function RemoveRecordTemplate({
  template,
  open,
  setOpen,
  action,
  setRecordTemplates,
  handleOpen,
  setIsDeleted,
  disabled,
}) {
  const { mutate: deleteTemplate, isLoading: isDeleting } = useDeleteRecordTemplate();

  const { enqueueSnackbar } = useSnackbar();

  const handleRemove = (templateId) => {
    deleteTemplate(templateId, {
      onSuccess: () => {
        setRecordTemplates((prevTemplates) =>
          prevTemplates.filter((template) => template.id !== templateId)
        );
        setIsDeleted(true);
        enqueueSnackbar(<SnackbarMessage message="Template is deleted successfully !" />, {
          variant: "success",
        });

        const timeoutId = setTimeout(() => {
          setIsDeleted(false);
        }, 2000);
        return () => clearTimeout(timeoutId);
      },
    });
  };
  return (
    <>
      {disabled ? (
        <IconButton
          aria-label="delete"
          onClick={() => handleOpen("delete")}
          disabled
          className={"MuiButtonBase-root-MuiIconButton-root.Mui-disabled"}
        >
          <DeleteIcon fontSize="small" />
        </IconButton>
      ) : (
        <Tooltip title="Delete">
          <IconButton
            id="deleteButtonId"
            aria-label="delete"
            onClick={() => handleOpen("delete")}
            className="action-MuiSvgIcon-root"
          >
            <DeleteIcon fontSize="small" />
          </IconButton>
        </Tooltip>
      )}

      {open === true && action === "delete" && (
        <ActionConfirm
          templateId={template.id}
          onConfirm={handleRemove}
          open={open}
          setOpen={setOpen}
          message={"Are you sure you want to delete this template ?"}
          action={"delete"}
        />
      )}
    </>
  );
}

export default WithPermission(RemoveRecordTemplate, [
  "Maintenance_RecordTemplates_Action_Delete_View",
  "Maintenance_RecordTemplates_Action_Delete_Execute",
]);
