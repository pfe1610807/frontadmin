import React from "react";
import { IconButton, MenuItem, Tooltip } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import { useNavigate } from "react-router-dom";
import "assets/styles.css";
import WithPermission from "components/permissions/withPermission";

function UpdateRecordTemplate({ template, disabled }) {
  const navigate = useNavigate();

  const handleEdit = (templateId) => {
    navigate("/record-templates/edit", { state: { templateId: templateId } });
  };

  return (
    <>
      {template.isPublished || disabled ? (
        <IconButton
          aria-label="update"
          disabled
          onClick={() => handleEdit(template.id)}
          className={"disabled-button"}
        >
          <EditIcon fontSize="small" />
        </IconButton>
      ) : (
        <Tooltip title="Update">
          <IconButton
            aria-label="update"
            onClick={() => handleEdit(template.id)}
            className="action-MuiSvgIcon-root"
          >
            <EditIcon fontSize="small" />
          </IconButton>
        </Tooltip>
      )}
    </>
  );
}

export default WithPermission(UpdateRecordTemplate, [
  "Maintenance_RecordTemplates_Action_Edit_View",
  "Maintenance_RecordTemplates_Action_Edit_Execute",
]);
