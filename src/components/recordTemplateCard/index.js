import React, { useState } from "react";
import { Card, CardContent, Typography, Box, CardMedia, CardActions, Divider } from "@mui/material";
import "assets/styles.css";
function RecordTemplateCard({
  template,
  setRecordTemplates,
  setIsPublished,
  setIsDeleted,
  actionComponents,
}) {
  const [open, setOpen] = useState(false);
  const [action, setAction] = useState("");

  // For delete or publish
  const handleOpen = (action) => {
    setOpen(true);
    setAction(action);
  };

  return (
    <Card
      sx={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        marginRight: "3%",
        marginLeft: "3%",
      }}
    >
      <CardMedia sx={{ margin: "3%" }}>
        <Box
          component="img"
          src={template.image}
          alt="Record Template"
          sx={{
            width: "100%",
            height: "100px",
            borderRadius: "6px",
          }}
        />
      </CardMedia>
      <Divider sx={{ margin: 0 }} />
      <CardContent sx={{ textAlign: "left", flexGrow: 1 }}>
        <Typography
          variant="body2"
          sx={{
            fontWeight: "bold",
            whiteSpace: "nowrap",
            overflow: "hidden",
            textOverflow: "ellipsis",
          }}
        >
          {template.title}
        </Typography>
        <Typography variant="body2">
          Last update:
          <br /> {new Date(template.updatedAt).toLocaleString()}
        </Typography>
      </CardContent>
      <CardActions disableSpacing sx={{ justifyContent: "flex-end" }}>
        {actionComponents.map((ActionComponent, i) => (
          <ActionComponent
            key={i}
            template={template}
            open={open}
            setOpen={setOpen}
            action={action}
            setRecordTemplates={setRecordTemplates}
            handleOpen={handleOpen}
            setIsPublished={setIsPublished}
            setIsDeleted={setIsDeleted}
          />
        ))}
      </CardActions>
    </Card>
  );
}

export default RecordTemplateCard;
