import React, { useState } from "react";
import { IconButton, Tooltip } from "@mui/material";
import ActionConfirm from "./actionConfirm";
import "assets/styles.css";
import { usePublishRecordTemplate } from "api/recordTemplateApi";
import WithPermission from "components/permissions/withPermission";
import PublishIcon from "@mui/icons-material/Publish";
import { useSelector } from "react-redux";
import JwtParser from "components/auth/JWTParser";
import { useSnackbar } from "notistack";
import SnackbarMessage from "components/toast/snackbarMessage";

function PublishRecordTemplate({
  template,
  open,
  setOpen,
  action,
  setRecordTemplates,
  handleOpen,
  setIsPublished,
  disabled,
}) {
  const { mutate: publishTemplate } = usePublishRecordTemplate();

  const { socket } = useSelector((state) => state.socket);
  const { token } = useSelector((state) => state?.auth);
  const decodedToken = JwtParser.parseJwtToken(token?.token);

  const { enqueueSnackbar } = useSnackbar();
  const [published, setPublished] = useState(template.isPublished);

  const handlePublish = (templateId) => {
    publishTemplate(templateId, {
      onSuccess: () => {
        setRecordTemplates((prevTemplates) =>
          prevTemplates.map((template) =>
            template.id === templateId ? { ...template, publish: true } : template
          )
        );
        setIsPublished(true);
        setPublished(true);
        enqueueSnackbar(<SnackbarMessage message="Template is published successfully !" />, {
          variant: "success",
        });

        if (socket) {
          socket.emit("sendNotification", {
            message: "Trigger notification",
            entity: "RecordTemplate",
            entityId: templateId,
            userId: decodedToken?.userId,
          });
        }
        const timeoutId = setTimeout(() => {
          setIsPublished(false);
        }, 2000);
        return () => clearTimeout(timeoutId);
      },
    });
  };

  return (
    <>
      {template.isPublished || published || disabled ? (
        <IconButton
          id="publishButtonId"
          aria-label="publish"
          disabled
          className={"disabled-button"}
          onClick={() => handleOpen("publish")}
        >
          <PublishIcon fontSize="medium" />
        </IconButton>
      ) : (
        <Tooltip title="Publish">
          <IconButton
            id="publishButtonId"
            aria-label="publish"
            onClick={() => handleOpen("publish")}
            className="action-MuiSvgIcon-root"
          >
            <PublishIcon fontSize="medium" />
          </IconButton>
        </Tooltip>
      )}
      {open === true && action === "publish" && (
        <ActionConfirm
          templateId={template.id}
          onConfirm={handlePublish}
          open={open}
          setOpen={setOpen}
          message={"Are you sure you want to publish this template ?"}
          action={"publish"}
        />
      )}
    </>
  );
}

export default WithPermission(PublishRecordTemplate, [
  "Maintenance_RecordTemplates_Action_Publish_View",
  "Maintenance_RecordTemplates_Action_Publish_Execute",
]);
