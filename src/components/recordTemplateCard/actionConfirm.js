import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import CancelIcon from "@mui/icons-material/Cancel";
import PublishIcon from "@mui/icons-material/Publish";
import DeleteIcon from "@mui/icons-material/Delete";
import "assets/styles.css";

export default function ActionConfirm({ templateId, onConfirm, open, setOpen, message, action }) {
  const handleClose = () => {
    setOpen(false);
  };

  const handleAction = async () => {
    await onConfirm(templateId);
    setOpen(false);
  };

  const getActionButtons = () => {
    switch (action) {
      case "publish":
        return (
          <>
            <Button
              id="confirmPublishId"
              onClick={handleAction}
              startIcon={<PublishIcon />}
              variant="contained"
              sx={{ marginLeft: "80px" }}
              className="submitButton"
            >
              Publish
            </Button>
            <Button
              onClick={handleClose}
              startIcon={<CancelIcon />}
              variant="outlined"
              sx={{ color: "#42647b", borderColor: "#42647b", height: " 40px" }}
            >
              Cancel
            </Button>
          </>
        );
      case "delete":
        return (
          <>
            <Button
              id="confirmDeleteId"
              onClick={handleAction}
              startIcon={<DeleteIcon />}
              variant="contained"
              className="deleteButton"
            >
              Delete
            </Button>
            <Button
              onClick={handleClose}
              startIcon={<CancelIcon />}
              variant="outlined"
              sx={{ color: "#b00020", borderColor: "#b00020", height: " 40px" }}
            >
              Cancel
            </Button>
          </>
        );
      default:
        return null;
    }
  };

  return (
    <Dialog open={open} onClose={handleClose} id="confirmationDialogId">
      <div style={{ textAlign: "center", padding: "20px" }}>
        <DialogTitle>Confirm {action}</DialogTitle>
        <DialogContent>
          <DialogContentText>{message}</DialogContentText>
        </DialogContent>
        <DialogActions style={{ justifyContent: "center" }}>{getActionButtons()}</DialogActions>
      </div>
    </Dialog>
  );
}
