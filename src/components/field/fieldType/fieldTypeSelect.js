import React from "react";
import { Grid, Select, MenuItem, ListItemIcon, Typography } from "@mui/material";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import EventIcon from "@mui/icons-material/Event";
import ArrowDropDownCircleIcon from "@mui/icons-material/ArrowDropDownCircle";
import ShortTextIcon from "@mui/icons-material/ShortText";
import NotesIcon from "@mui/icons-material/Notes";

function FieldTypeSelect({ field, handleFieldTypeChange, darkMode }) {
  return (
    <Grid item xs={18} sm={6}>
      <Select
        id="select-field"
        variant="standard"
        value={field.fieldType}
        onChange={(e) => {
          handleFieldTypeChange(field.fieldOrder, e.target.value);
        }}
        className="formField"
        fullWidth
      >
        <MenuItem value="" disabled>
          <Typography variant="inherit">Field type</Typography>
        </MenuItem>
        <MenuItem value="text">
          <Typography component={"span"} variant="inherit">
            <ListItemIcon>
              <ShortTextIcon className="action-MuiSvgIcon-root" />
            </ListItemIcon>
            One Line Text
          </Typography>
        </MenuItem>
        <MenuItem value="MutlipleLine">
          <Typography component={"span"} variant="inherit">
            <ListItemIcon>
              <NotesIcon className="action-MuiSvgIcon-root" />
            </ListItemIcon>
            Mutliple Line Text
          </Typography>
        </MenuItem>
        <MenuItem value="multichoice">
          <Typography component={"span"} variant="inherit">
            <ListItemIcon>
              <RadioButtonUncheckedIcon className="action-MuiSvgIcon-root" />
            </ListItemIcon>
            Multiple Choice
          </Typography>
        </MenuItem>
        <MenuItem value="checkbox">
          <Typography component={"span"} variant="inherit">
            <ListItemIcon>
              <CheckBoxIcon className="action-MuiSvgIcon-root" />
            </ListItemIcon>
            Checkbox
          </Typography>
        </MenuItem>
        <MenuItem value="datetime">
          <Typography component={"span"} variant="inherit">
            <ListItemIcon>
              <EventIcon className="action-MuiSvgIcon-root" />
            </ListItemIcon>
            Datetime Picker
          </Typography>
        </MenuItem>
        <MenuItem value="Dropdown">
          <Typography component={"span"} variant="inherit">
            <ListItemIcon>
              <ArrowDropDownCircleIcon className="action-MuiSvgIcon-root" />
            </ListItemIcon>
            Dropdown
          </Typography>
        </MenuItem>
      </Select>
    </Grid>
  );
}

export default FieldTypeSelect;
