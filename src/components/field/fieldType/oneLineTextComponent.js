import { TextField } from "@mui/material";
import React from "react";

function OneLineTextComponent({ field, isAdminComponent, handleAnswerChange, response }) {
  return (
    <>
      {isAdminComponent === false ? (
        <>
          <TextField
            label="Answer"
            value={response}
            variant="standard"
            type="text"
            fullWidth
            onChange={(e) => {
              handleAnswerChange(field.id, e.target.value);
            }}
          />
        </>
      ) : (
        <>
          {/* Admin interface (record template) */}
          <TextField disabled fullWidth placeholder="Text answer ..." />
        </>
      )}
    </>
  );
}

export default OneLineTextComponent;
