import React, { useState, useEffect, useRef } from "react";
import Checkbox from "@mui/material/Checkbox";
import { Grid, TextField } from "@mui/material";
import DraggableOptions from "../options/draggableOptions";
import AddOption from "../options/addOption";

const CheckboxComponent = ({
  field,
  fields,
  setFields,
  isAdminComponent,
  handleAnswerChange,
  response,
}) => {
  const newOptionRef = useRef(null);
  useEffect(() => {
    if (newOptionRef.current) {
      newOptionRef.current.focus(); // focus the cursor on the new added option
    }
  }, [field.options.length]);

  const [checkedOptions, setCheckedOptions] = useState([]);
  const handleCheckboxChange = (optionName) => {
    if (checkedOptions.includes(optionName))
      setCheckedOptions((prevOptions) => prevOptions.filter((option) => option !== optionName));
    else setCheckedOptions((prevOptions) => [...prevOptions, optionName]);
  };

  useEffect(() => {
    if (isAdminComponent === false) handleAnswerChange(field.id, checkedOptions.join(", "));
  }, [checkedOptions]);

  useEffect(() => {
    if (response) setCheckedOptions(response.split(", "));
  }, [response]);
  return (
    <>
      {isAdminComponent === false ? (
        <>
          {/* User interface (record) */}
          {field.options.map((option) => (
            <Grid key={option.id} style={{ display: "flex", alignItems: "center" }}>
              <Checkbox
                name={option.optionName}
                onChange={(e) => handleCheckboxChange(e.target.name)}
                checked={checkedOptions.includes(option.optionName)}
              />

              <TextField
                value={option.optionName}
                variant="standard"
                InputProps={{ disableUnderline: true }}
              />
            </Grid>
          ))}
        </>
      ) : (
        <>
          <DraggableOptions
            field={field}
            fields={fields}
            setFields={setFields}
            newOptionRef={newOptionRef}
          />
          <Grid container spacing={2}>
            <Grid item xs>
              <Grid className="addOptionTextField">
                <Checkbox name="Add option" disabled />
                <AddOption field={field} setFields={setFields} />
              </Grid>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
};

export default CheckboxComponent;
