import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material";
import RadioButtonUncheckedIcon from "@mui/icons-material/RadioButtonUnchecked";
import React, { useRef, useEffect } from "react";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import "assets/styles.css";
import DraggableOptions from "../options/draggableOptions";
import AddOption from "../options/addOption";
import CheckBox from "@mui/icons-material/CheckBox";

function MultiChoiceComponent({
  field,
  fields,
  setFields,
  isAdminComponent,
  response,
  handleChange,
  selectedOption,
}) {
  const newOptionRef = useRef(null);
  useEffect(() => {
    if (newOptionRef.current) {
      newOptionRef.current.focus();
    }
  }, [field.options.length]);

  return (
    <>
      {isAdminComponent === false ? (
        <>
          {field.options.map((option) => (
            <Grid key={option.id} style={{ display: "flex", alignItems: "center" }}>
              <Radio
                key={option.id}
                value={option.optionName}
                checked={option.optionName === response}
                onChange={handleChange}
              />
              <TextField
                value={option.optionName}
                variant="standard"
                InputProps={{ disableUnderline: true }}
              />{" "}
            </Grid>
          ))}
        </>
      ) : (
        <>
          {/* Admin interface (record template) */}
          <DraggableOptions
            field={field}
            fields={fields}
            setFields={setFields}
            newOptionRef={newOptionRef}
          />
          <Grid container spacing={2}>
            <Grid item xs>
              <Grid className="addOptionTextField">
                <Radio
                  icon={<RadioButtonUncheckedIcon />}
                  checkedIcon={<CheckBoxIcon />}
                  checked={false}
                  disabled
                  className="grayRadio"
                />
                <AddOption field={field} setFields={setFields} />
              </Grid>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

export default MultiChoiceComponent;
