import React, { useEffect, useState } from "react";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import dayjs from "dayjs";

function DateTimePickerComponent({ isAdminComponent, handleAnswerChange, field, response }) {
  const [responseDate, setResponseDate] = useState(null);

  useEffect(() => {
    if (response) {
      setResponseDate(dayjs(response.replace(" ", "T")));
    }
  }, [response]);
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DemoContainer components={["DateTimePicker"]}>
        {isAdminComponent === false ? (
          <>
            {/* User interface (record) */}
            <DateTimePicker
              label="Datetime"
              value={responseDate}
              onChange={(value) =>
                handleAnswerChange(field.id, value.format("YYYY-MM-DD HH:mm:ss"))
              }
              viewRenderers={{
                hours: null,
                minutes: null,
                seconds: null,
              }}
            />
          </>
        ) : (
          <>
            {/* Admin interface (record template) */}
            <DateTimePicker label="MM/DD/YYYY hh:mm:ss aa" disabled />
          </>
        )}
      </DemoContainer>
    </LocalizationProvider>
  );
}

export default DateTimePickerComponent;
