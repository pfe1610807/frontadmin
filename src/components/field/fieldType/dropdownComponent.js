import React, { useRef, useEffect } from "react";
import { FormControl, Grid, IconButton, InputLabel, MenuItem, Select } from "@mui/material";
import DraggableOptions from "../options/draggableOptions";
import AddOption from "../options/addOption";
import { ArrowDropDownIcon } from "@mui/x-date-pickers";
function DropdownComponent({ field, fields, setFields, isAdminComponent, handleChange, response }) {
  const newOptionRef = useRef(null);
  useEffect(() => {
    if (newOptionRef.current) {
      newOptionRef.current.focus(); // focus the cursor on the new added option
    }
  }, [field.options.length]);

  return (
    <>
      {isAdminComponent === false ? (
        <>
          {/* User interface (record) */}
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Select</InputLabel>
            <Select
              labelId="answer-label"
              label={field.fieldName}
              value={response}
              variant="standard"
              fullWidth
              onChange={handleChange}
              IconComponent={() => (
                <IconButton size="small">
                  <ArrowDropDownIcon />
                </IconButton>
              )}
            >
              <MenuItem value="" disabled>
                Select an option
              </MenuItem>
              {field.options.map((option) => (
                <MenuItem key={option.optionName} value={option.optionName}>
                  {option.optionName}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </>
      ) : (
        <>
          {/* Admin interface (record template) */}
          <DraggableOptions
            field={field}
            fields={fields}
            setFields={setFields}
            newOptionRef={newOptionRef}
          />
          <Grid container spacing={2}>
            <Grid item xs>
              <Grid className="addOptionTextField">
                {" "}
                <AddOption field={field} setFields={setFields} />
              </Grid>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

export default DropdownComponent;
