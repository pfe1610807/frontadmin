import React from "react";
import { TextField, Grid, Typography, InputAdornment, IconButton } from "@mui/material";
import ViewHeadlineIcon from "@mui/icons-material/ViewHeadline";

function MultipleLineComponent({ label, field, isAdminComponent, handleAnswerChange, response }) {
  return (
    <>
      {isAdminComponent === false ? (
        <>
          <TextField
            value={response}
            variant="standard"
            multiline
            rows={4}
            fullWidth
            onChange={(e) => {
              handleAnswerChange(field.id, e.target.value);
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton>
                    <ViewHeadlineIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </>
      ) : (
        <>
          <Grid>
            <Grid item xs={12}>
              <Typography variant="subtitle1">{label}</Typography>
              <TextField
                multiline
                rows={4}
                variant="outlined"
                fullWidth
                disabled
                placeholder="Text answer ..."
              />
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

export default MultipleLineComponent;
