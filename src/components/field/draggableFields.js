import React, { useState, useEffect } from "react";
import { Grid } from "@mui/material";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import Field from "./field";

function DraggableFields({ fields, setFields }) {
  const [isDraggable, setIsDraggable] = useState(false);

  const toggleDraggable = () => {
    setIsDraggable((prevState) => !prevState);
  };

  const handleDragEnd = (result) => {
    if (!result.destination || isDraggable) {
      return;
    }

    const newFields = [...fields];
    const draggedField = newFields.splice(result.source.index, 1)[0];
    newFields.splice(result.destination.index, 0, draggedField);

    const dropZone = result.destination.droppableId;
    const isInFormGroup = dropZone.includes("FormGroup");

    if (isInFormGroup) {
      setIsDraggable(false);
    }

    setFields(newFields.map((field, index) => ({ ...field, fieldOrder: index })));
    setIsDraggable(false);
  };

  useEffect(() => {
    const handleMouseUp = () => {
      setIsDraggable(false);
    };
    document.addEventListener("mouseup", handleMouseUp);
    return () => {
      document.removeEventListener("mouseup", handleMouseUp);
    };
  }, []);
  const handleFieldDragStart = (e) => {
    e.preventDefault();
  };

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <Droppable droppableId="fields">
        {(provided) => (
          <Grid
            {...provided.droppableProps}
            ref={provided.innerRef}
            onDragStart={handleFieldDragStart}
          >
            {fields
              .sort((a, b) => (a.fieldOrder > b.fieldOrder ? 1 : -1))
              .map((field, i) => (
                <Draggable
                  key={field.fieldOrder}
                  draggableId={field.fieldOrder.toString()}
                  index={i}
                  isDragDisabled={!isDraggable}
                >
                  {(provided) => (
                    <Grid
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      ref={provided.innerRef}
                    >
                      <Field
                        field={field}
                        fields={fields}
                        setFields={setFields}
                        toggleDraggable={toggleDraggable}
                        key={i}
                      ></Field>
                    </Grid>
                  )}
                </Draggable>
              ))}
            {provided.placeholder}
          </Grid>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default DraggableFields;
