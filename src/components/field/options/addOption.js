import React from "react";
import { TextField } from "@mui/material";

const AddOption = ({ field, setFields }) => {
  const handleAddOption = () => {
    const fieldOrder = field.fieldOrder;
    setFields((prevFields) =>
      prevFields.map((field) =>
        field.fieldOrder === fieldOrder
          ? {
              ...field,
              options: [
                ...field.options,
                {
                  optionName: "Option",
                  optionOrder: field.options.length,
                },
              ].map((option, index) => ({ ...option, optionOrder: index })),
            }
          : field
      )
    );
  };

  return (
    <TextField fullWidth label="Add option" variant="standard" onFocus={() => handleAddOption()} />
  );
};

export default AddOption;
