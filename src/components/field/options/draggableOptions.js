import { Grid } from "@mui/material";
import "assets/styles.css";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import Option from "./option";
import React, { useState, useEffect } from "react";

function DraggableOptions({ newOptionRef, field, fields, setFields }) {
  const [isDraggable, setIsDraggable] = useState(false);

  const toggleDraggable = () => {
    setIsDraggable((prevState) => !prevState);
  };
  const handleDragEnd = (result) => {
    if (!result.destination || isDraggable) {
      return;
    }

    const newOptions = Array.from(field.options);
    const [draggedOption] = newOptions.splice(result.source.index, 1);
    newOptions.splice(result.destination.index, 0, draggedOption);
    const updatedOptions = newOptions.map((option, index) => ({
      ...option,
      optionOrder: index,
    }));

    const fieldOrder = field.fieldOrder;
    setFields((prevFields) =>
      prevFields.map((field) =>
        field.fieldOrder === fieldOrder
          ? {
              ...field,
              options: updatedOptions.map((option, index) => ({
                ...option,
                optionOrder: index,
              })),
            }
          : field
      )
    );
    setIsDraggable(false);
  };

  useEffect(() => {
    const handleMouseUp = () => {
      setIsDraggable(false);
    };
    document.addEventListener("mouseup", handleMouseUp);
    return () => {
      document.removeEventListener("mouseup", handleMouseUp);
    };
  }, []);
  const handleFieldDragStart = (e) => {
    e.preventDefault();
  };

  return (
    <DragDropContext onDragEnd={handleDragEnd}>
      <Droppable droppableId="options">
        {(provided) => (
          <Grid
            {...provided.droppableProps}
            ref={provided.innerRef}
            onDragStart={handleFieldDragStart}
          >
            {field.options
              .sort((a, b) => (a.optionOrder > b.optionOrder ? 1 : -1))
              .map((option, i) => (
                <Draggable
                  key={option.optionOrder}
                  draggableId={option.optionOrder.toString()}
                  index={i}
                >
                  {(provided) => (
                    <Grid
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      ref={provided.innerRef}
                    >
                      <Option
                        option={option}
                        key={i}
                        field={field}
                        newOptionRef={newOptionRef}
                        fields={fields}
                        setFields={setFields}
                        toggleDraggable={toggleDraggable}
                      ></Option>
                    </Grid>
                  )}
                </Draggable>
              ))}
            {provided.placeholder}
          </Grid>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default DraggableOptions;
