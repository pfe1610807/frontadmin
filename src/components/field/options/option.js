import React from "react";
import Checkbox from "@mui/material/Checkbox";
import ClearIcon from "@mui/icons-material/Clear";
import { Grid, IconButton, TextField } from "@mui/material";
import DragIndicatorIcon from "@mui/icons-material/DragIndicator";
import "assets/styles.css";

import { FormControlLabel, Radio } from "@mui/material";
import {
  RadioButtonUnchecked as RadioButtonUncheckedIcon,
  CheckBox as CheckBoxIcon,
} from "@mui/icons-material";

const Option = ({ option, field, setFields, newOptionRef, toggleDraggable, darkMode }) => {
  const fieldOrder = field.fieldOrder;
  const handleOptionNameChange = (optionOrder, newOptionName) => {
    setFields((prevFields) =>
      prevFields.map((field) =>
        field.fieldOrder === fieldOrder
          ? {
              ...field,
              options: field.options.map((option) =>
                option.optionOrder === optionOrder
                  ? {
                      ...option,
                      optionName: newOptionName,
                    }
                  : option
              ),
            }
          : field
      )
    );
  };

  const handleRemoveOption = (optionOrder) => {
    setFields((prevFields) =>
      prevFields.map((field) =>
        field.fieldOrder === fieldOrder
          ? {
              ...field,
              options: field.options
                .filter((option) => option.optionOrder !== optionOrder)
                .map((option, index) => ({ ...option, optionOrder: index })),
            }
          : field
      )
    );
  };
  return (
    <>
      {field.fieldType == "checkbox" ? (
        <>
          {" "}
          <Grid
            container
            key={option.optionOrder}
            justifyContent="space-between"
            alignItems="center"
            sx={{ mr: 5 }}
            spacing={2}
          >
            <Grid item>
              <IconButton
                onClick={(e) => {
                  toggleDraggable();
                }}
                className="action-MuiSvgIcon-root"
              >
                <DragIndicatorIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <Checkbox name={option.optionName} disabled />
            </Grid>
            <Grid item xs>
              <TextField
                variant="standard"
                value={option.optionName}
                fullWidth
                onChange={(e) => handleOptionNameChange(option.optionOrder, e.target.value)}
                inputRef={option.optionOrder == field.options.length - 1 ? newOptionRef : null}
              />
            </Grid>
            <Grid item>
              <IconButton
                aria-label="delete"
                onClick={() => handleRemoveOption(option.optionOrder)}
                className="action-MuiSvgIcon-root"
              >
                <ClearIcon />
              </IconButton>
            </Grid>
          </Grid>
        </>
      ) : field.fieldType === "Dropdown" ? (
        <>
          <Grid container alignItems="center" key={option.optionOrder} spacing={2}>
            <Grid item>
              <IconButton
                className="action-MuiSvgIcon-root"
                onClick={(e) => {
                  toggleDraggable();
                }}
              >
                <DragIndicatorIcon />
              </IconButton>
            </Grid>
            <Grid item xs>
              <TextField
                value={option.optionName}
                onChange={(e) => handleOptionNameChange(option.optionOrder, e.target.value)}
                inputRef={option.optionOrder == field.options.length - 1 ? newOptionRef : null}
                variant="standard"
                type="text"
                fullWidth
              />
            </Grid>
            <Grid item>
              <IconButton
                className="action-MuiSvgIcon-root"
                onClick={() => handleRemoveOption(option.optionOrder)}
              >
                <ClearIcon />
              </IconButton>
            </Grid>
          </Grid>
        </>
      ) : field.fieldType === "multichoice" ? (
        <>
          <Grid container spacing={2} key={option.optionOrder} alignItems="center">
            <Grid item>
              <IconButton
                onClick={(e) => {
                  toggleDraggable();
                }}
                className="action-MuiSvgIcon-root"
              >
                <DragIndicatorIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <FormControlLabel
                control={
                  <Radio
                    icon={<RadioButtonUncheckedIcon />}
                    checkedIcon={<CheckBoxIcon />}
                    value={`option${option.optionOrder}`}
                    checked={option.option === `option${option.optionOrder}`}
                    className="grayRadio"
                  />
                }
                label=""
              />
            </Grid>
            <Grid item xs>
              <TextField
                variant="standard"
                type="text"
                fullWidth
                className="formField"
                value={option.optionName}
                onChange={(e) => handleOptionNameChange(option.optionOrder, e.target.value)}
                inputRef={option.optionOrder == field.options.length - 1 ? newOptionRef : null}
              />
            </Grid>
            <Grid item>
              <IconButton
                aria-label="delete"
                onClick={() => handleRemoveOption(option.optionOrder)}
                className="action-MuiSvgIcon-root"
              >
                <ClearIcon />
              </IconButton>
            </Grid>
          </Grid>
        </>
      ) : null}
    </>
  );
};

export default Option;
