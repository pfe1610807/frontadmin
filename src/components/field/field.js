import { Typography, TextField, FormGroup, Grid, Switch, IconButton, Box } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import React, { useState } from "react";
import MultiChoiceComponent from "./fieldType/multiChoiceComponent";
import DropdownComponent from "./fieldType/dropdownComponent";
import MultipleLineComponent from "./fieldType/multipleLineComponent";
import FieldTypeSelect from "./fieldType/fieldTypeSelect";
import OneLineTextComponent from "./fieldType/oneLineTextComponent";
import DateTimePickerComponent from "./fieldType/dateTimePickerComponent";
import CheckboxComponent from "./fieldType/checkboxComponent";
import DragHandleIcon from "@mui/icons-material/DragHandle";
import { motion } from "framer-motion";
import "assets/styles.css";

const spring = {
  type: "spring",
  stiffness: 700,
  damping: 30,
};

function Field({ field, fields, setFields, toggleDraggable }) {
  const handleFieldNameChange = (fieldOrder, newFieldName) => {
    setFields((prevFields) =>
      prevFields.map((field) =>
        field.fieldOrder === fieldOrder ? { ...field, fieldName: newFieldName } : field
      )
    );
  };
  const handleRemoveField = (fieldOrder) => {
    setFields((prevFields) => {
      const remainingFields = prevFields
        .filter((field) => field.fieldOrder !== fieldOrder)
        .map((field, index) => ({ ...field, fieldOrder: index }));
      return remainingFields.length == 0
        ? [
            {
              fieldType: "text",
              fieldName: "",
              isMandatory: false,
              fieldOrder: prevFields.length,
              options: [],
            },
          ]
        : remainingFields;
    });
  };
  //To remove the options when the field type changes and the new field type does not support options
  const handleFieldTypeChange = (fieldOrder, newFieldType) => {
    setFields((prevFields) =>
      prevFields.map((field) => {
        if (field.fieldOrder === fieldOrder) {
          let updatedField = { ...field, fieldType: newFieldType };

          // Check if the newFieldType supports options
          if (
            newFieldType !== "multichoice" &&
            newFieldType !== "Dropdown" &&
            newFieldType !== "checkbox"
          ) {
            updatedField = { ...updatedField, options: [] };
          }

          return updatedField;
        }
        return field;
      })
    );
  };

  const handleSwitchChange = (fieldOrder) => {
    setFields((prevFields) =>
      prevFields.map((field) =>
        field.fieldOrder === fieldOrder ? { ...field, isMandatory: !field.isMandatory } : field
      )
    );
  };
  const handleAddField = () => {
    setFields((prevFields) =>
      [
        ...prevFields,
        {
          fieldType: "text",
          fieldName: "",
          isMandatory: false,
          fieldOrder: prevFields.length,
          options: [],
        },
      ].map((field, index) => ({ ...field, fieldOrder: index }))
    );
  };
  const [isOn, setIsOn] = useState(false);

  const toggleSwitch = () => setIsOn(!isOn);
  return (
    <>
      <FormGroup className="formContainer" style={{ paddingTop: 0, marginBottom: 0 }}>
        <Grid container justifyContent="center" alignItems="flex-end" style={{ marginTop: "-1%" }}>
          <IconButton
            size="large"
            className="action-MuiSvgIcon-root"
            onClick={() => {
              toggleDraggable();
            }}
          >
            <DragHandleIcon />
          </IconButton>
        </Grid>

        <Grid container spacing={10} style={{ cursor: "grab" }}>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Field"
              variant="outlined"
              type="text"
              fullWidth
              className="formField"
              value={field.fieldName}
              onChange={(e) => handleFieldNameChange(field.fieldOrder, e.target.value)}
            />
          </Grid>

          <Grid item xs={12} sm={6}>
            <FieldTypeSelect field={field} handleFieldTypeChange={handleFieldTypeChange} />
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          {field.fieldType === "multichoice" ? (
            <MultiChoiceComponent field={field} fields={fields} setFields={setFields} />
          ) : field.fieldType === "Dropdown" ? (
            <DropdownComponent field={field} fields={fields} setFields={setFields} />
          ) : field.fieldType === "MutlipleLine" ? (
            <MultipleLineComponent />
          ) : field.fieldType === "text" ? (
            <OneLineTextComponent />
          ) : field.fieldType === "datetime" ? (
            <DateTimePickerComponent isDisabled={true} />
          ) : field.fieldType === "checkbox" ? (
            <CheckboxComponent field={field} fields={fields} setFields={setFields} />
          ) : null}
        </Grid>
        <br />
        <Grid className="bottomActions">
          <Typography variant="body2" margin={1}>
            Mandatory
          </Typography>

          {/* <Switch
            className="icon-button"
            checked={field.isMandatory}
            onChange={(e) => handleSwitchChange(field.fieldOrder, e.target.value)}
          /> */}
          <div
            className="switch"
            data-isOn={field.isMandatory}
            // onClick={(e) => handleSwitchChange(field.fieldOrder, e.target.value)}
            onClick={(e) => handleSwitchChange(field.fieldOrder)}

          >
            <motion.div className="handle" layout transition={spring} />
          </div>
          <IconButton
            aria-label="delete"
            onClick={() => handleRemoveField(field.fieldOrder)}
            className="action-MuiSvgIcon-root"
          >
            <DeleteIcon />
          </IconButton>
        </Grid>
      </FormGroup>
      <Box display="flex" justifyContent="center">
        <IconButton aria-label="add" onClick={handleAddField} className="action-MuiSvgIcon-root">
          <AddCircleIcon
            fontSize="large"
            style={{
              display: fields.indexOf(field) === fields.length - 1 ? "block" : "none",
            }}
          />
        </IconButton>
      </Box>
    </>
  );
}

export default Field;
