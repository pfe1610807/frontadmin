const { Builder, By, until } = require("selenium-webdriver");

describe("Publish Templates Test", () => {
  let driver;

  beforeAll(async () => {
    driver = await new Builder().forBrowser("chrome").build();
    await driver.get("http://localhost:3002/record-templates");
    await driver.executeScript(() => {
      const token =
        "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MTMxIiwic3ViIjoidXNlcnN5c3RlbSIsInVzZXJJZCI6IjhjN2Q4NTE4LWEzNmEtNDBkNS04YjZjLWJmMjYxOWVjNTVjMSIsIkZpcnN0TG9naW4iOjAsImlhdCI6MTcyMjA3NjEwMSwiZXhwIjoxNzIyMDg2OTAxfQ.oN3ir7K48QA2ElST1ezhYkWkomYftv1TVqmKsQqDD964GDqYMhD8C_PWBhVIgJs8_DR7aYSl1xpMRHxVkkOhiQ";
      const event = new CustomEvent("setToken", { detail: token });
      window.dispatchEvent(event);
    });
    await driver.navigate().refresh();
  }, 20000);

  afterAll(async () => {
    await driver.quit();
  });

  test("Should display success snackbar when publishing the template", async () => {
    // Wait for the publish button to be located and visible
    let publishButton = await driver.wait(until.elementLocated(By.id("publishButtonId")));
    await driver.wait(until.elementIsVisible(publishButton));

    console.log("Publish button located: ", publishButton);
    // Click the publish button
    await publishButton.click();

    // // Wait for the confirmation dialog to appear
    let confirmationDialog = await driver.wait(until.elementLocated(By.id("confirmationDialogId")));
    await driver.wait(until.elementIsVisible(confirmationDialog));
    console.log("Confirmation dialog located: ", confirmationDialog);

    // Find the confirm button in the dialog by its id
    let confirmButton = await driver.wait(until.elementLocated(By.id("confirmPublishId")));
    await driver.wait(until.elementIsVisible(confirmButton));

    // Click the confirm button
    await confirmButton.click();

    // Wait for the success message to appear
    let successMessage = await driver.wait(until.elementLocated(By.id("snackbar-success-message")));
    await driver.wait(until.elementIsVisible(successMessage));

    console.log("Success message located: ", successMessage);
    let successMessageText = await successMessage.getText();
    expect(successMessageText).toContain("Template is published successfully !");
  }, 20000);
});
