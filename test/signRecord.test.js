// const { Builder, By, until } = require("selenium-webdriver");
// const chrome = require("selenium-webdriver/chrome");

// (async function SignRecordTemplate() {
//   let driver = await new Builder().forBrowser("chrome").build();
//   try {
//     // Charger la page de l'application
//     await driver.get("http://localhost:3002/records");
//     await driver.executeScript(() => {
//       const token ="eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI1MzI5Iiwic3ViIjoidXNlcnN5c3RlbSIsInVzZXJJZCI6ImRmNWZhOTI3LWVhYzgtNGUzNy04YWY3LTYxN2Q0MWQ4Y2UwMSIsIkZpcnN0TG9naW4iOjAsImlhdCI6MTcxOTU4MTgyNCwiZXhwIjoxNzE5NTkyNjI0fQ.FMBVYGgDft9ELkQbfycpszvFU1Zx7HhQofir3RwkJ7-_8SDo-u7hPWi99tHWna_N84Yh02lfuiMZ5rBpgmHUKA"
//         const event = new CustomEvent("setToken", { detail: token });
//         window.dispatchEvent(event);
//       });
//       await driver.navigate().refresh();
//     let signButton = await driver.wait(until.elementLocated(By.id("signButton")));
//     await signButton.click();

//   } catch (error) {
//     console.error("Failed to click sign button:", error);
//   } finally {
//     await driver.quit();
//   }
// })();
const { Builder, By, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");

describe("Sign Record Test", () => {
  let driver;

  beforeAll(async () => {
    driver = await new Builder().forBrowser("chrome").build();
    await driver.get("http://localhost:3002/records");
    await driver.executeScript(() => {
      const token =
        "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MTMxIiwic3ViIjoidXNlcnN5c3RlbSIsInVzZXJJZCI6IjhjN2Q4NTE4LWEzNmEtNDBkNS04YjZjLWJmMjYxOWVjNTVjMSIsIkZpcnN0TG9naW4iOjAsImlhdCI6MTcyMjA3NjEwMSwiZXhwIjoxNzIyMDg2OTAxfQ.oN3ir7K48QA2ElST1ezhYkWkomYftv1TVqmKsQqDD964GDqYMhD8C_PWBhVIgJs8_DR7aYSl1xpMRHxVkkOhiQ";
      const event = new CustomEvent("setToken", { detail: token });
      window.dispatchEvent(event);
    });
    await driver.navigate().refresh();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("should navigate to the correct URL after clicking the button", async () => {
    let signButton = await driver.wait(until.elementLocated(By.id("signButton")));
    await driver.wait(until.elementIsVisible(signButton));
    await signButton.click();
    let successDeleteMessage = await driver.wait(
      until.elementLocated(By.id("snackbar-success-message")),
      10000
    );
    await driver.wait(until.elementIsVisible(successDeleteMessage));
    let successDeleteMessageText = await successDeleteMessage.getText();
    expect(successDeleteMessageText).toContain("Record signed successfully");
  }, 20000);
});
