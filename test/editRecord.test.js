const { Builder, By, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");

describe("Edit Record Test", () => {
  let driver;

  beforeAll(async () => {
    driver = await new Builder().forBrowser("chrome").build();
    await driver.get("http://localhost:3002/records");
    await driver.executeScript(() => {
      const token =
        "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MTMxIiwic3ViIjoidXNlcnN5c3RlbSIsInVzZXJJZCI6IjhjN2Q4NTE4LWEzNmEtNDBkNS04YjZjLWJmMjYxOWVjNTVjMSIsIkZpcnN0TG9naW4iOjAsImlhdCI6MTcyMjA3NjEwMSwiZXhwIjoxNzIyMDg2OTAxfQ.oN3ir7K48QA2ElST1ezhYkWkomYftv1TVqmKsQqDD964GDqYMhD8C_PWBhVIgJs8_DR7aYSl1xpMRHxVkkOhiQ";
      const event = new CustomEvent("setToken", { detail: token });
      window.dispatchEvent(event);
    });
    await driver.navigate().refresh();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("should navigate to the correct URL after clicking the button", async () => {
    await driver.wait(until.elementLocated(By.id("iconButton_edit_Record_Answers")));
    let editButton = await driver.findElement(By.id("iconButton_edit_Record_Answers"));
    await driver.wait(until.elementIsVisible(editButton));
    await editButton.click();
    await driver.wait(until.urlContains("record/edit"));

    // Validate the URL
    let url = await driver.getCurrentUrl();
    console.log("Current URL: ", url);
    if (url.includes("record/edit")) {
      console.log("Test Passed: Navigated to the correct URL");
    } else {
      console.log("Test Failed: URL did not change as expected");
    }
  }, 20000);
});
