const { Builder, By, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");

describe("Export PDF Test", () => {
  let driver;

  beforeAll(async () => {
    driver = await new Builder().forBrowser("chrome").build();
    await driver.get("http://localhost:3002/records");
    await driver.executeScript(() => {
      const token =
        "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MTMxIiwic3ViIjoidXNlcnN5c3RlbSIsInVzZXJJZCI6IjhjN2Q4NTE4LWEzNmEtNDBkNS04YjZjLWJmMjYxOWVjNTVjMSIsIkZpcnN0TG9naW4iOjAsImlhdCI6MTcyMjA3NjEwMSwiZXhwIjoxNzIyMDg2OTAxfQ.oN3ir7K48QA2ElST1ezhYkWkomYftv1TVqmKsQqDD964GDqYMhD8C_PWBhVIgJs8_DR7aYSl1xpMRHxVkkOhiQ";
      const event = new CustomEvent("setToken", { detail: token });
      window.dispatchEvent(event);
    });
    await driver.navigate().refresh();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("should navigate to the correct URL after clicking the button", async () => {
    let button = await driver.wait(until.elementLocated(By.id("export-pdf-button")));
    await driver.wait(until.elementIsVisible(button));

    await button.click();
    let successDeleteMessage = await driver.wait(
      until.elementLocated(By.id("snackbar-success-message"))
    );
    await driver.wait(until.elementIsVisible(successDeleteMessage));
    let successDeleteMessageText = await successDeleteMessage.getText();
    expect(successDeleteMessageText).toContain("PDF exported successfully");
  }, 20000);
});
