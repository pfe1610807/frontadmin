const { Builder, By, until } = require("selenium-webdriver");

describe("Preview Templates Test", () => {
  let driver;

  beforeAll(async () => {
    driver = await new Builder().forBrowser("chrome").build();
    await driver.get("http://localhost:3002/record-templates");
    await driver.executeScript(() => {
      const token =
        "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiI2MTMxIiwic3ViIjoidXNlcnN5c3RlbSIsInVzZXJJZCI6IjhjN2Q4NTE4LWEzNmEtNDBkNS04YjZjLWJmMjYxOWVjNTVjMSIsIkZpcnN0TG9naW4iOjAsImlhdCI6MTcyMjA3NjEwMSwiZXhwIjoxNzIyMDg2OTAxfQ.oN3ir7K48QA2ElST1ezhYkWkomYftv1TVqmKsQqDD964GDqYMhD8C_PWBhVIgJs8_DR7aYSl1xpMRHxVkkOhiQ";
      const event = new CustomEvent("setToken", { detail: token });
      window.dispatchEvent(event);
    });
    await driver.navigate().refresh();
  }, 20000);

  afterAll(async () => {
    await driver.quit();
  });

  test("Should display the preview dialog", async () => {
    // preview button
    let previewButton = await driver.wait(until.elementLocated(By.id("preview-template")));
    await driver.wait(until.elementIsVisible(previewButton));
    console.log("Preview button located: ", previewButton);
    await previewButton.click();
    await driver.sleep(2000);

    // preview dialog title
    let previewDialogTitle = await driver.wait(until.elementLocated(By.id("preview-dialog-title")));
    await driver.wait(until.elementIsVisible(previewDialogTitle));
    console.log("Preview dialog title located: ", previewDialogTitle);
    let dialogTitleText = await previewDialogTitle.getText();
    expect(dialogTitleText).toContain("Preview");

    // preview close
    let closeButton = await driver.findElement(By.id("preview-close"));
    await closeButton.click();
    await driver.wait(until.stalenessOf(previewDialogTitle));
    console.log("Preview dialog closed successfully.");
  }, 20000);
});
