const { Builder, By, until } = require("selenium-webdriver");
async function setupDriver() {
  let driver = await new Builder().forBrowser("chrome").build();
  return driver;
}

module.exports = { setupDriver, By, until };
