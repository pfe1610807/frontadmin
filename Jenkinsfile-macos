pipeline {
    agent any
    environment {
        SONARQUBE_SERVER = 'http://localhost:9000'
        SONARQUBE_CREDENTIALS_ID = credentials('SonarQube')
        DOCKER_REGISTRY = 'nexus.acams.no:8086'
        DOCKER_CREDENTIALS_ID = 'nexus'
        VERSION = '1.0'
    }
    stages {
        stage('Install Dependencies') {
            steps {
                script {
                    sh 'npm install'
                }
            }
        }
        stage('Build') {
            steps {
                script {
                    sh 'CI="" npm run build'
                }
            }
        }
        stage('Test') {
            steps {
                script {
                    echo 'Running frontend tests...'
                    // sh 'npx jest preview-template.test.js'
                }
            }
        }
        stage('SonarQube Analysis') {
            steps {
                withCredentials([string(credentialsId: 'SonarQube', variable: 'SONARQUBE_CREDENTIALS_ID')]) {
                    echo 'Running SonarQube analysis...'
                    sh "sonar-scanner -Dsonar.host.url=${SONARQUBE_SERVER} -Dsonar.login=${SONARQUBE_CREDENTIALS_ID}"
                }
            }
        }
        stage('Build Frontend Docker Image') {
            steps {
                script {
                    COMMIT_ID = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
                    IMAGE_TAG = "${VERSION}-${COMMIT_ID}"
                    echo "Building Docker image with tag: maintenance_frontend:${IMAGE_TAG}"
                    sh "docker build -t maintenance_frontend:${IMAGE_TAG} -f Dockerfile ."
                }
            }
        }
        stage('Push Docker Image to Nexus') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: DOCKER_CREDENTIALS_ID, usernameVariable: 'DOCKER_USERNAME', passwordVariable: 'DOCKER_PASSWORD')]) {
                        sh "docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD} ${DOCKER_REGISTRY}"
                        sh "docker tag maintenance_frontend:${IMAGE_TAG} ${DOCKER_REGISTRY}/maintenance_frontend:${IMAGE_TAG}"
                        sh "docker push ${DOCKER_REGISTRY}/maintenance_frontend:${IMAGE_TAG}"
                        sh "docker tag maintenance_frontend:${IMAGE_TAG} ${DOCKER_REGISTRY}/maintenance_frontend:latest"
                        sh "docker push ${DOCKER_REGISTRY}/maintenance_frontend:latest"
                    }
                }
            }
        }
    }
}
