# Use the official Node.js image to build and run the React application
FROM node:16

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Install cross-env globally
RUN npm install -g cross-env

# Copy the rest of the application code
COPY . .

# Expose port 3002
EXPOSE 3002

# Command to run the app
CMD ["npm", "start"]
